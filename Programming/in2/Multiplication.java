package Programming.in2;

import java.util.Scanner;

public class Multiplication {
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Give us a number you want the table for.");
        int number = scan.nextInt();
        scan.close();

        for(int i = 0; i < 13; i++) {
            System.out.println(number + " * " + i + " = " + number*i);
        }
    } 
}
