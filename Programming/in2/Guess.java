package Programming.in2;

import java.util.Scanner;
import java.util.Random;

public class Guess {
    public static void main(String args[]) {
        Random rand = new Random();
        int number = rand.nextInt(101);
        Scanner scan = new Scanner(System.in);
        System.out.println("Guess the number");
        int guess = scan.nextInt();
        while(guess != number) {
            if(guess < number){
                System.out.println("Your number was to low, guess again.");
            }
            else{
                System.out.println("Your number was to high, guess again.");
            }
            guess = scan.nextInt();
        }
        scan.close();
        System.out.println("Congrats you guessed correctly.");
    }
}
