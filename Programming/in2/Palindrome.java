package Programming.in2;

import java.util.Scanner;

public class Palindrome {
    public static void main(String args[]) {
        int r, sum = 0, temp;
        Scanner scan = new Scanner(System.in);
        System.out.println("Give use a number to check");
        int n = scan.nextInt(); //Number to check if palindrome.
        scan.close();
        temp = n;
        while(n > 0){
            r = n%10;
            sum = (sum*10) + r;
            if(temp == sum) {
                System.out.println(n + " is palindrome");
            }
            else{
                System.out.println(n + " is not a palindrom");
            }
        }
    }
}
