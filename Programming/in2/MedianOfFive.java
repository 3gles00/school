package Programming.in2;

import java.util.Scanner;
import java.util.Arrays;

public class MedianOfFive {
    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);
        System.out.println("Median of five, give us five numbers");
        System.out.println("The first number:");
        int n1 = scan.nextInt();
        System.out.println("The second number:");
        int n2 = scan.nextInt();
        System.out.println("The third number:");
        int n3 = scan.nextInt();
        System.out.println("The fourth number:");
        int n4 = scan.nextInt();
        System.out.println("The fifth number:");
        int n5 = scan.nextInt();
        scan.close();

        int[] array = {n1, n2, n3, n4, n5};
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
        System.out.println(array[2]);
    }
}
