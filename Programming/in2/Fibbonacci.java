package Programming.in2;

import java.util.Arrays;
import java.util.Scanner;

public class Fibbonacci {
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.println("How many numbers do you want to print from the fibbonacci sequence");   
        int number = scan.nextInt();
        scan.close();
        int[] fibbi = new int[number];
        fibbi[0] = 1;
        fibbi[1] = 1;
        for(int i = 2; i < number; i++) {
            fibbi[i] = fibbi[i-1] + fibbi[i-2];
        }
        System.out.println(Arrays.toString(fibbi));
    }
}
