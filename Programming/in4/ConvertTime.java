package Programming.in4;

import java.util.Scanner;

public class ConvertTime {

    public static String convert(int time) {

        int h = time/3600;
        int m = (time%3600)/60;
        int s = time%60;
        String convert = h + " h " + m + " min " + s + " s";
        return convert;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Give us the amount of seconds you want to convert");
        int time = scan.nextInt();
        scan.close();

        System.out.println("The time converted is" + convert(time));
        
        
    }
}
