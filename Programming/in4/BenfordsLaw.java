package Programming.in4;

public class BenfordsLaw
{
    public static int firstnum(int x)
    {
        while (x>=10){
             x = x/10;
        }
        return x;
    }
    public static void main(String [] cmdLn)
    {
        int[] firstnumber = new int[10];
        int k = 0; 
        
        while (!StdIn.isEmpty()) 
        {
            int z = StdIn.readInt();
            int digit = firstnum(z); 
            firstnumber[digit]++;
            k++;
        }
        for (int i = 1; i<=9;i++)
        {
            StdOut.printf("%d: %5.3f%%\n", i , (firstnumber[i]*100.0)/k);
        }
    }
}
