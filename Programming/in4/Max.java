package Programming.in4;

import java.util.Scanner;

public class Max {    
    public static int max2 (int k,int l)
    {
        if (k>l){
            return k;
        }
        else 
        return l;
    }
     public static int max3 (int x , int y ,int z)
    {
        return max2(max2(x, y), z);
    }

   public static void main(String [] cmdLn)
   {
       Scanner scan = new Scanner(System.in);
       System.out.println("Give us 3 numbers to compare");
       int x = scan.nextInt(); 
       int y = scan.nextInt(); 
       int z = scan.nextInt();
       scan.close();
       
       System.out.println("The biggest number between " + x + " and " + y + " is " + max2(x,y));
       System.out.println("The largest numbers among all three is " + max3(x, y, z));
      
    }
}
