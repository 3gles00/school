package Programming.in4;

import java.util.Arrays;
import java.util.Scanner;

public class CheckSum {

    public static int f(int sum)
    {
        int a = sum * 2;
        int b = a / 10;
        int c = a % 10;

        sum = b + c;
        return sum;
    }
    /*
     * 
     */
    public static int checksum(int[] n)
    {
        int sum = 0;
        int l = 0;
        for (int k = 0; k < 10; k++)
        {   
            l++;
            if (l == 2)
            {
                n[k] = f(n[k]);
                l = 0;
            }
            else 
            {
                n[k] = n[k];
            }
            sum = sum + n[k];
        }
       
        System.out.println(sum);
        return sum;
    }
    public static boolean allowed(int[] P)
    {
        boolean b;
        if (checksum(P) % 10 != 0)
        {
            b = false;
        }
        else 
        {
            b = true;
        }

        return b;

    }
     public static int[] extended_account(int[] t)
    {
        int[] arr = new int [11];
        int sum = checksum(t);

        for (int i = 0; i < 10; i++)
        {
            arr[i] = t[i];
        }

        for (int j = 0; j < 10; j++)
        {
            if ((sum + j) % 10 == 0 )                {
                arr[10] = j;
            }
        }
        return arr;
    }
    public static void main(String [] cmdLn)
    {

        int[] N = new int [10];
        int n;
        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < 10; i++)
        {
            System.out.println("Give us the " + (i + 1)   + " number");
            n = sc.nextInt();

            N[i] = n;
        }
        sc.close();
        System.out.println(Arrays.toString(N));
        N = extended_account(N); 
        System.out.println(Arrays.toString(N));
    }
}
