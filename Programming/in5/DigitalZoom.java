package Programming.in5;

import java.awt.*;
import java.util.*;

public class DigitalZoom {
    public static void main(String[] cmdLn) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Name of the picture");
        String picName = scan.nextLine();
        scan.close();

        System.out.println("Ange ett zoomfaktor mellan 0 till 1");
        double zoomfaktor = scan.nextDouble();
        zoomfaktor = 1 / zoomfaktor;

        System.out.println("Ange ett x");
        double x = scan.nextDouble();

        System.out.println("Ange ett y");
        double y = scan.nextDouble();

        Picture pic = new Picture("Users/Haron/Desktop/git/school/Programming/in5/" + picName + ".jpg");
        pic.show();

        int width = pic.width();
        int height = pic.height();

        Picture pic2 = new Picture(width, height);
        Color black = new Color(0, 0, 0);

        for (int colpic2 = 0; colpic2 < width; colpic2++) {
            for (int radpic2 = 0; radpic2 < height; radpic2++) { // skapar det nya bildens ramar beroende på det x och y
                                                                 // värde som man insätter och zoomar in
                int colpic1 = (int) ((x * width) - (width / (2 * zoomfaktor)) + (colpic2 / zoomfaktor));
                int radpic1 = (int) ((y * height) - (height / (2 * zoomfaktor)) + (radpic2 / zoomfaktor));
                if (colpic1 >= width || radpic1 < 0 || radpic1 >= height || colpic1 < 0) {
                    pic2.set(colpic2, radpic2, black);
                } else {
                    pic2.set(colpic2, radpic2, pic.get(colpic1, radpic1));

                }
            }
        }
        pic2.show();
    }
}