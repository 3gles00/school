package Programming.in5;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class KamaSutraCipher
{
    private static String[] generaraNyckel()
    {
        // the funcktion StringBuilder can make it easier to edit Strings
        StringBuilder primary = new StringBuilder("abcdefghijklmnopqrstuvwxyz");
        StringBuilder[] nycklar = {new StringBuilder() ,  new StringBuilder()};

        Random r = new Random();
        boolean b = true;
        int i = 0; 
        while (b == true)
        {
            
            int s = r.nextInt()%primary.length();
            int k = Integer.MAX_VALUE%primary.length();
            i++;
            if (i < 26)
            {
                int variable = s & k;
                nycklar[i % 2].append(primary.charAt(variable));
                primary.deleteCharAt(variable);
                
            }
            else 
            {
                b = false;
            }
            
        }
        return new String[] {nycklar[0].toString() , nycklar[1].toString()};
    }
    public static String krypteringochavkryptering(String key1, String key2, String meddelande)
    {
        // the string converts to char array 
        char[] r = meddelande.toCharArray();

        // a ascii board generates for the swith between letters
        char[] ascii = new char[128];

         boolean b = true;
         int k = 0; 
         
         while (b == true)
         {
             k++;
             
             if (k < key1.length())
             {
                 ascii[key1.charAt(k)] = key2.charAt(k);
                 ascii[key2.charAt(k)] = key1.charAt(k);
             }
             else if (key1.length() == 32)
             {
                 continue;
             }
             else
             {
                 b = false;
             }
            }    
                
        return String.valueOf(r);
    }
    public static void main(String[] cmdLn)
    {
        // user types in word/sentence to encrypt and decrypt
        Scanner scan = new Scanner(System.in);
        System.out.println("put in a word to encrypt and decrypt");
        String meddelande = scan.nextLine();
        scan.close();
        
        
        // prints and generates random key for enkryption 
        // and decryption
        String[] nycklar = generaraNyckel();
        String kryptat = krypteringochavkryptering(nycklar[0], nycklar[1], meddelande);
        String avkryptat = krypteringochavkryptering(nycklar[0], nycklar[1], kryptat);

        // prints the awnsers
        System.out.println(Arrays.toString(nycklar));
        System.out.println(kryptat);
        System.out.println(avkryptat);
    }
}