package Programming.in5;

import java.util.Scanner;

public class SafePassword
{
    public static boolean isSafe(String password) {
        boolean safe=false, length = false, big = false, small = false, numbers = false, symbols = false;
        char[] pass = password.toCharArray();
        char[] Big = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        char[] Small = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        char[] Numbers = "1234567890".toCharArray();
        char[] Symbols = "!#¤%&/()=?`§½@£$€{[]}".toCharArray();

        if(pass.length > 8){
            length = true;
        }

        for(int i = 0; i < pass.length; i++) {
            for(int j = 0; j < Big.length; j++) {
                if(pass[i] == Big[j]){
                    big = true;
                }
            }
            for(int j = 0; j < Small.length; j++) {
                if(pass[i] == Small[j]){
                    small = true;
                }
            }
            for(int j = 0; j < Numbers.length; j++) {
                if(pass[i] == Numbers[j]){
                    numbers = true;
                }
            }
            for(int j = 0; j < Symbols.length; j++) {
                if(pass[i] == Symbols[j]){
                    symbols = true;
                }
            }
        }

        if((length == true && big == true && small == true && numbers == true && symbols == true)){
            safe = true;
        }
        return safe;
    }
    public static void main(String[] cmdLn){
        
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter password to compare");
        String  strPwd = scan.nextLine();
        scan.close();
        System.out.println(isSafe(strPwd));
    }
}