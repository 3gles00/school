package Programming.in3;

import java.util.Scanner;

public class CheckDate {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Give us a year to check");
        int year = scan.nextInt();
        System.out.println("Give us a month to check");
        int month = scan.nextInt();
        System.out.println("Give us a day to check");
        int day = scan.nextInt();
        scan.close();

        boolean vMonth = false;
        boolean vDay = false;
        int validDaysInMonth = 0;
        int feb = 0;

        if(year%4 == 0)
        {
            feb = 29;
        }
        else
        {
            feb = 28;
        }
        if(12 > month && month> 0) {
            vMonth = true;
        }
        if(month == 4 || month == 6 || month == 9 || month == 11){
            validDaysInMonth = 30;
        }
        else if(month == 2) {
            validDaysInMonth = feb;
            vDay = true;
        }
        else validDaysInMonth = 31;
        if(day > 0 && day < validDaysInMonth) vDay = true;

        if(vDay == true && vMonth == true){
            System.out.println(year + " " + month + " " + day +" is Valid");
        }
        else{
            System.out.println(year + " " + month + " " + day +" is Invalid");
        }
    }
}
