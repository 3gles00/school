package Programming.in3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Arrays;

public class Names {
    
    public static void main(String[] args) {
        try 
        {
            //Change path if file is exported.
            File file = new File("\\Users\\Haron\\Desktop\\git\\school\\Programming\\in3\\assets\\name.txt");
            Scanner reader = new Scanner(file);
            String[] names = new String[10];
            int count = 0;
            while(reader.hasNextLine()){
                names[count] = reader.nextLine();
                count++;
            }
            reader.close();
            Arrays.sort(names);
            System.out.println(Arrays.toString(names));
        } catch(FileNotFoundException e){
            System.out.println("An error occured");
            e.printStackTrace();
        }
    }
}
