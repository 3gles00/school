package Programming.in3;

import java.util.Random;

public class BirthDayProblem {
    public static void main(String[] args) {
        Random rand = new Random();
        int[] studentsBirthDay = new int[365];
        int count = 0;

        // Loading up the array 
        for(int i = 0; i < 365; i++) {
            studentsBirthDay[i] = 0;
        }

        boolean same = false;
        while(same == false) {
            int student = rand.nextInt(365);
            studentsBirthDay[student]++;
            count++;
            if(studentsBirthDay[student] == 2){
                System.out.println(count + " students before 2 had the same birthday on the " + student + " th day");
                same = true;
            }
        }
    }
}
