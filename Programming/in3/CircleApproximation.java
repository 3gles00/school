package Programming.in3;

import java.util.Scanner;

public class CircleApproximation {
    public static void main(String[] cmdLn)
    {
        System.out.println("Give us a number");

        Scanner obj = new Scanner(System.in);
        int n = obj.nextInt();
        obj.close();

        StdDraw.setXscale(-4,4); //Sets the window
        StdDraw.setYscale(-4,4);

        for(int i = 0; i < n; i++) {
            double corners = 3 * (Math.pow(2,i)); //3*2^n = corners
            double angle = 2 * (Math.PI/corners); //Gets the angle for the figure

            for(int j = 0; j < corners; j++) { //Prints the sides of the figure
                double x1 = Math.cos(angle * j); 
                double y1 = Math.sin(angle * j);
                double x2 = Math.cos(angle * ( j + 1)); //One unit away from the first set of coordinates
                double y2 = Math.sin(angle * ( j + 1));

                StdDraw.line(x1, y1, x2, y2);
            }
        }
    }
}
