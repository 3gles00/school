package Programming.in3;

import java.util.Random;
import java.util.Arrays;

public class EigenVector {
    public static void main(String[] args){
        Random rand = new Random();
        int[][] array = new int[3][3];
        int[] vector = new int[3];

        System.out.println("Our 3x3 matrix");
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                array[i][j] = rand.nextInt(4);
            }
            vector[i] = rand.nextInt(4);
            System.out.println(Arrays.toString(array[i]));
        }
        System.out.println();
        System.out.println("Our Vecotor");
        System.out.print(Arrays.toString(vector));

    }
}
