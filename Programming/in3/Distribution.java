package Programming.in3;

import java.util.Scanner;
import java.util.Random;

public class Distribution {
    public static void main(String[] args){
        Random rand = new Random();
        Scanner scan = new Scanner(System.in);
        System.out.println("Give us a limit for random");
        int limit = scan.nextInt();
        scan.close();
        int[] numbs = new int[15];

        for(int i = 0; i < limit; i++){
            int randomNumber = rand.nextInt(15) + 1;
            for(int j = 0; j < 15; j++){
                if(j+1 == randomNumber) {
                    numbs[j]++;
                }
            }
        }

        for(int k = 0; k < 15; k++){
            float temp1 = numbs[k];
            float temp2 = limit;
            float percentage = temp1/temp2;
            System.out.println(k+1 + " was generated " + percentage + "%");
    
        }

    }
}