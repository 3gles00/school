package Programming.in1;

import java.util.Scanner;

public class LeapYear {
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.println("We are going to find out if the year is a leap year");
        System.out.println("Give us a year");
        int year = scan.nextInt();
        scan.close();

        if(year%4 == 0) {
            if(year%100 == 0) {
                System.out.println("Year is not a leapyear");
            }
            else{
                System.out.println("Year is a leap year");
            }
        }
        else{
            System.out.println("Year is not a leapyear");
        }
    }
}
