package Programming.in1;

import java.util.Scanner;

public class TwoThreeFive {
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Give us a number.");
        int numb = scan.nextInt();
        scan.close();

        System.out.println();
        if(numb%5 == 0) {
            System.out.println("Number is dividable with 5");
        }
        if(numb%3 == 0) {
            System.out.println("Number is dividable with 3");
        }
        if(numb%2 == 0) {
            System.out.println("Number is dividable with 2");
        }
        if(numb%2 == 0 && numb%3 == 0 && numb%5 == 0) {
            System.out.println("Number is dividable with 5, 3, 2");
        }
    }
}
