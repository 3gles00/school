package Programming.in1;

import java.util.Scanner;

public class Crash {
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.println("We are going to calculate the break stretch");
        System.out.println("Give us the speed of the car.");
        int velocity = scan.nextInt();
        System.out.println("give us the friction constant of the road");
        float friction = scan.nextFloat();
        scan.close();

        System.out.println("The stretch of the crash was " + Math.pow(velocity, 2)/(250*friction));
    }    
}
