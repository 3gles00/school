package Programming.in1;

import java.util.Scanner;

public class Time2 {
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.println("We are gonna convert an amount of seconds to days, hours and minutes");
        System.out.println("Give us the amount of seconds you want us to convert");
        int input = scan.nextInt();
        scan.close();

        int days = input/86400;
        input = input%86400;
        int hours = input/3600;
        input = input%3600;
        int minutes = input/60;
        input = input%60;
        int seconds = input;
        System.out.println(days + " days " + hours + " hours " + minutes + " minutes " + seconds + " seconds ");
    }
}
