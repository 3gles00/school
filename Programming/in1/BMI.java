package Programming.in1;

import java.util.Scanner;

public class BMI {
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Hi we are going to calculate your personal Body Mass Index (BMI)");
        System.out.println("Please give us your weight in kilograms");
        double weight = scan.nextDouble();
        System.out.println("Please give us your height in meters");
        double height = scan.nextDouble();
        scan.close();

        System.out.println("Your BMI is: " + (weight)/(height*height));
    }
}
