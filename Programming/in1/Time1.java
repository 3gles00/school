package Programming.in1;

import java.util.Scanner;

public class Time1 {
    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);
        System.out.println("We are going to calculate the amount of seconds you have given us.");
        System.out.println("Give us the amount of days (if less then 1 say 0)");
        int days = scan.nextInt();
        System.out.println("Give us the amount of hours (if less then one say 0");
        int hours = scan.nextInt();
        System.out.println("Give us the amount of minutes (if less then one say 0");
        int minutes = scan.nextInt();
        System.out.println("Give us the amount of seconds");
        int seconds = scan.nextInt();
        scan.close();

        System.out.println("The time you have given us totals: " + days*86400 + hours*3600 + minutes*60 + seconds + " seconds");
    }
}
