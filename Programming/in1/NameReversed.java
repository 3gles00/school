package Programming.in1;

import java.util.Scanner;

public class NameReversed {
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Insert First and Last Name");
        String name = scan.nextLine();
        char[] converter = name.toCharArray();
        scan.close();
        StringBuilder firstname = new StringBuilder();
        StringBuilder surrname = new StringBuilder();

        int spaceCount = 0;
        for(int i = 0; i < name.length(); i++) {
            if(converter[i] == 32) spaceCount++;
            if(spaceCount != 0) {
                firstname.append(converter[i]);
            }
            else{
                surrname.append(converter[i]);
            }
        }

        System.out.println(surrname.toString() + "," + firstname.toString());

    }
}