// Online C compiler to run C program online
#include <stdio.h>
#include <math.h>

struct complex{
    double re, im;
};

struct complex conj1(struct complex z){
    z.im = -1*(z.im);
    return z;
}

struct complex add(struct complex z1, struct complex z2){
    z1.re = z1.re + z2.re;
    z1.im = z1.im + z2.im;
    return z1;
}

struct complex minus(struct complex z1, struct complex z2){
    z1.re = z1.re - z2.re;
    z1.im = z1.im - z2.im;
    return z1;
}

struct complex multiply(struct complex z1, struct complex z2){
    double a = z1.re;
    double b = z1.im;
    double c = z2.re;
    double d = z2.im;
    
    z1.re = a*c - b*d;
    z1.im = a*d + b*c;
    return z1;
}

struct complex divide(struct complex z1, struct complex z2){
    double a = z1.re;
    double b = z1.im;
    double c = z2.re;
    double d = z2.im;
    
    z1.re = (a*c + b*d)/(pow(c, 2) + pow(d, 2));
    z1.im = (b*c - a*d)/(pow(c, 2) + pow(d, 2));
    return z1;
}
struct complex reciprocal(struct complex z){
    double re = z.re;
    double im = z.im;
    z.re = re/(pow(re,2)+pow(im,2));
    z.im = -1*im/(pow(re, 2) + pow(im, 2));
    return z;
};

double magnitude(struct complex z){
   return sqrt(pow(z.re,2)+pow(z.im, 2));
};


int main(){
    struct complex z1, z2;

    printf("Re for Z1: ");
    scanf("%lf", &z1.re);
    printf("Im for Z1: ");
    scanf("%lf", &z1.im);
    printf("Re for Z2: ");
    scanf("%lf", &z2.re);
    printf("Im for Z2: ");
    scanf("%lf", &z2.im);
    
    printf("Z1 + Z1 = %f + %f i\n", add(z1,z1).re, add(z1,z1).im);
    printf("Z1 - Z2 = %f + %f i\n", minus(z1,z2).re, minus(z1,z2).im);
    printf("Z1 * Z2 = %f + %f i\n", multiply(z1, z2).re, multiply(z1, z2).im);
    printf("Z1 / Z2 = %f + %f i\n", divide(z1, z2).re, divide(z1, z2).im);
    printf("Magnitude of Z1 = %f \n", magnitude(z1));
    printf("Reciprocal of Z1 = %f + %f i\n", reciprocal(z1).re, reciprocal(z1).im);
    return 0;
}