#include <stdio.h>
#include <math.h>

struct complex{
    double re, im;
}z;

double magnitude(struct complex z){
   return sqrt(pow(z.re, 2) + pow(z.im, 2));
};

double avgMag(struct complex z[], int size){
    double sum = 0;
    for(int i = 0; i < size; i++){
        sum += magnitude(z[i]);
    };
    return sum/size;
};

double minMag(struct complex z[], int size){
    double min = magnitude(z[0]);
    for(int i = 1; i < size; i++){
        if(min > magnitude(z[i]))
            min = magnitude(z[i]);
    }
    return min;
}

double maxMag(struct complex z[], int size){
    double max = magnitude(z[0]);
    for(int i = 1; i < size; i++){
        if(max < magnitude(z[i])){
            max = magnitude(z[i]);
        }
    }
    return max;
}

/*
double mean(struct complex z[]){
    return (magnitude(z[49]) + magnitude(z[50]))/2 ;
}*/

double variance(struct complex z[], int size){
    double sum = 0;
    double avg = avgMag(z, size);
    for(int i = 0; i < (int) size; i++){
        sum += pow(magnitude(z[i]) - avg, 2);
    }
    return sum/size;
}

int main(){
    
    struct complex z[100]; 
    int size = (int) (sizeof(z)/sizeof(z[0]));
    
    int i;
    for(i = 0; i < 100; i++){
        z[i].re = (double)10/(1+i);
        z[i].im = (double)7/(1+i);
    }
    
    printf("Average magnitude of the list: %f\n", avgMag(z, size));
    printf("Minimum magnitude of the list: %lf\n", minMag(z, size));
    printf("Maximum magnitude of the list: %lf\n", maxMag(z, size));
    printf("Variance in the magnitude of the list: %lf", variance(z, size));
    return 0;
}