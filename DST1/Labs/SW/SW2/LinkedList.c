#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

/* Constructor for the LinkedList
LinkedList contains an ID, sensordata
and a pointer to the next item in the list.
*/
struct LinkedList {
    int id;
    double sensorData;
    struct LinkedList * next;
};

/* printList is a recursive function while it iterates
throught the list printing the Items ID and SensorData. */
void printList(struct LinkedList *list) {
    if (list != NULL) {
        printf("id: %d sensorData: %lf \n", list -> id, list -> sensorData);
        printList(list -> next);
    }
}

/* insertFirst will add the new element in the begining of the list. */
void insertFirst(struct LinkedList ** first, struct LinkedList * el) {
    el ->next = *first;
    *first=el;
}
    
/* isMember returns an int based on if the elements */
int isMember(struct LinkedList **first, struct LinkedList *el) {
    struct LinkedList *temp = *first;
    while (temp !=NULL) {
        if (temp == el) {
             return 1;
        }
        temp=temp ->next;
    }
    return 0;
}

/*
Create List Item only creates items to be be used in the list
pointer for Item.
*/
struct LinkedList * createListItem(int id, double sensorData) {
    struct LinkedList * temp;
    temp=(struct LinkedList *) malloc(sizeof(struct LinkedList));
    if(temp != NULL){
        temp ->id=id;
        temp ->sensorData=sensorData;
    }
    else printf("Memory is full");
    return temp;
}

/* Function remove already exists in the Liberary
removeListItem takes in the first item in the list and the element you
want to remove and iterates throught the list to override it effectivly 
removing it from the cycle.
Note that the ListItem is not deleted from the memory! */
void removeListItem(struct LinkedList ** first, struct LinkedList * el) {
    if (isMember(first, el) ==0) {
        printf("Element is not in the list");
    }
    else{
        if (*first == el) {
            *first = (*first) ->next;
            return;
        }

        struct LinkedList *temp = *first;
        while ((temp -> next) !=el) {
            temp=temp ->next;
        }

        (temp -> next) = (temp -> next -> next);
    }
}

/* Read Sensor creates an item with random Sensor Data */
struct LinkedList * readSensor(int id) {
    struct LinkedList * temp = (struct LinkedList *) malloc(sizeof(struct LinkedList));
    if(temp != NULL){
        (temp -> id)=id;
        (temp -> sensorData)=((rand() % 10000) / 10000.0);
    }
    else printf("Memory is full");
    return temp;
}

/* Get Address is a supportive function that find the address if the id exists in the list*/
struct LinkedList * getAddress(struct LinkedList * first, int id) {
    while (first !=NULL) {
        if ((first -> id)==id) {
            return first;
        }

        (first)=(first -> next);
    }
    return NULL;
}

/* Free Memory frees the space so you don't run out of memory*/
void freeMemory(struct LinkedList **first) {
    struct LinkedList *list = *first;
    struct LinkedList *temp;

    while (list !=NULL) {
        temp = list;
        list = list ->next;
        free(temp);
    }
    *first = list;
}

/* Find Max will find the maximum sensorData value and return it*/
struct LinkedList * findMax(struct LinkedList *first){
    struct LinkedList *itr = first;
    struct LinkedList *temp = (struct LinkedList *) malloc(sizeof(struct LinkedList));
    if(temp != NULL){
    temp -> sensorData = 0;
        while(itr != NULL){
            if((itr -> sensorData) > (temp -> sensorData)){
                temp -> sensorData = itr -> sensorData;
                temp -> id = itr -> id;
            }
            itr = itr -> next;
        }
    }
    else printf("Memory is full");
    return temp;
}

/* Sort Ascend sorts the List from low to high Sensor Data */
struct LinkedList * sortAscend(struct LinkedList * first) {
    if (first == NULL) {
        printf("The list is empty");
    }
    else if (first -> next == NULL) {
        return first;
    }
    struct LinkedList * temp;
    struct LinkedList * max;
    temp = NULL;

    while (first != NULL) {
        max=findMax(first);
        insertFirst(& temp, max);
        removeListItem(& first, getAddress(first, max -> id));
    }
    freeMemory(&first);
    first=temp;
    return first;
}

void main() {
    time_t now;
    srand((unsigned int) time(& now));
    int size = 0;
    struct LinkedList * list = NULL;

    //Edge scenario no items in the list
    printf("\nEdge Scenario Test: No Items \n");
    printList(list);

    struct LinkedList * nodeA = createListItem(size++, 5);
    struct LinkedList * nodeB = createListItem(size++, 4);
    struct LinkedList * nodeC = createListItem(size++, 6);
    struct LinkedList * nodeD = createListItem(size++, 8);

    // Insert First Test
    insertFirst(&list, nodeA);
    insertFirst(&list, nodeB);
    insertFirst(&list, nodeC);
    insertFirst(&list, nodeD);
    
    printf("\nInsert First Test \n");
    printList(list);

    // Remove Test
    printf("\nRemove Test \n");   
    removeListItem(&list,nodeA);
    printList(list);

    // Read Sensor test
    struct LinkedList *nodeR1 = readSensor(size++);
    struct LinkedList *nodeR2 = readSensor(size++);
    struct LinkedList *nodeR3 = readSensor(size++);
    struct LinkedList *nodeR4 = readSensor(size++);

    insertFirst(&list, nodeR1);
    insertFirst(&list, nodeR2);
    insertFirst(&list, nodeR3);
    insertFirst(&list, nodeR4);
    printf("\nRead Sensor Test\n");
    printList(list);

    // sortAscend test
    struct LinkedList *list2 = sortAscend(list);
    printf("\nSort Ascend Test \n");
    printList(list2);
}