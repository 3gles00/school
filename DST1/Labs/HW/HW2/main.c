#include "at91sam3x8.h"

#include "system_sam3x.h"

#include "stdio.h"
#define AT91_PIOC_DIFSR (AT91_CAST(AT91_REG * ) 0x400E1484)
#define AT91_PIOD_DIFSR (AT91_CAST(AT91_REG * ) 0x400E1484)
#define AT91C_PIOD_AIMER (AT91_CAST(AT91_REG * ) 0x400E14B0)
#define AT91C_PIOD_SCDR (AT91_CAST(AT91_REG * ) 0x400E148C)



int func();
void config();

void delay(int a);
unsigned char Read_Status_Display();
void Write_Command_2_Display(unsigned char Command);
void Write_Data_2_Display(unsigned char Data);
void Init_Display(void);
void key_pressed();
void displayClear();


int func() {
  int value = -1;

  * AT91C_PIOD_OER = (1 << 2); // Output Enable REG Rows
  * AT91C_PIOC_OER = (7 << 7); // Output Enable REG Columns
  * AT91C_PIOD_CODR = (1 << 2); // Clear Output Data REG Rows 
  * AT91C_PIOC_SODR = (7 << 7); // Set Ouput Disable REG Columns
  * AT91C_PIOC_ODR = (15 << 2); // Output Disable REG Columns

  for (int i = 0; i < 3; i++) {
    * AT91C_PIOC_CODR = (1 << (7 + i)); // 
    for (int j = 0; j < 4; j++) {

      if ((( * AT91C_PIOC_PDSR & (1 << (2 + j))) == 0)) {
        value = ((j * 3) + (i + 1));

      }
    }
    * AT91C_PIOC_SODR = (7 << (7 + i));
  }
  * AT91C_PIOC_ODR = (7 << 7);
  if(value != -1)
  printf("\n%d\n", value);
  return value;

}

void config() {
  * AT91C_PMC_PCER = (3 << 13);

  * AT91C_PIOC_PER = (0x3FC);
  * AT91C_PIOD_PER = (1 << 2); // Enable pin 27 and 25
  * AT91C_PIOC_PER = (0xFF000); // enable pin 44 - 51
  // * AT91C_PIOC_PER = (1<<1); // Enable pin 33 replaces pin 47
  * AT91C_PIOD_OER = (0x5); //configure pin 25 and 27 as output
  * AT91C_PIOC_OER = (0xFF000); // Enables Data Bus 
  * AT91C_PIOC_OER = (1 << 1); // Enables 33 replaces 47
   *AT91_PIOC_DIFSR = (0x3BC); // Bounce for pins 34-37 and 39-41;
}

unsigned char Read_Status_Display(void) {
  unsigned char Temp;
  * AT91C_PIOC_ODR = (0xFF << 2); // Enable Databus Input
//  * AT91C_PIOC_SODR = (1 << 1);
  * AT91C_PIOC_SODR = (1 << 13); 
  * AT91C_PIOC_CODR = (1 << 12);
  * AT91C_PIOC_OER = (1 << 14);
  * AT91C_PIOC_SODR = (1 << 14);
  * AT91C_PIOC_SODR = (1 << 17);
  * AT91C_PIOC_CODR = (1 << 15);
  * AT91C_PIOC_CODR = (1 << 16);
  
 /* * AT91C_PIOC_PER = (1 << 1);
  * AT91C_PIOC_OER = (1 << 1);
  * AT91C_PIOC_CODR = (1 << 1); */
  delay(10);
  Temp = * AT91C_PIOC_PDSR >> 2;
  * AT91C_PIOC_SODR = (0x19 << 12); // Pins 47, 48, 51
  //* AT91C_PIOC_SODR = (1 << 1); // Pins 33

  * AT91C_PIOC_CODR = (1 << 13); // DIR as Input

  return Temp;

}

void Write_Command_2_Display(unsigned char Command) {
  while ((Read_Status_Display() & (3)) != 3); //Wait until you recive a command
  * AT91C_PIOC_CODR = (0xFF << 2);

  * AT91C_PIOC_SODR = ((unsigned int) Command) << 2; // Command set to bus

//  * AT91C_PIOC_CODR = (1 << 1);
  
  * AT91C_PIOC_CODR = (1 << 13);
  * AT91C_PIOC_CODR = (1 << 12);

  * AT91C_PIOC_OER = (0xFF << 2);

  * AT91C_PIOC_SODR = (1 << 14);
  * AT91C_PIOC_CODR = (1 << 15);
  * AT91C_PIOC_CODR = (1 << 17);
  delay(10);
  * AT91C_PIOC_SODR = (1 << 15);
  * AT91C_PIOC_SODR = (1 << 17);
  * AT91C_PIOC_SODR = (1 << 12); // 74Chip = Pin 51 is inverse

  * AT91C_PIOC_ODR = (0xFF << 2); // Databus set as input
}

void Write_Data_2_Display(unsigned char Data) {
  while ((Read_Status_Display() & (3)) != 3);
  * AT91C_PIOC_CODR = (0xFF << 2); //clear databus
  * AT91C_PIOC_SODR = ((unsigned int) Data << 2);
//  * AT91C_PIOC_CODR = (1 << 1);
  * AT91C_PIOC_CODR = (1 << 13);
  * AT91C_PIOC_CODR = (1 << 12);

  * AT91C_PIOC_OER = (0xFF << 2); // Set pins to out
  * AT91C_PIOC_CODR = (1 << 14); //Clear cd;
  * AT91C_PIOC_CODR = (1 << 15); //clear chip select;
  * AT91C_PIOC_CODR = (1 << 17); //Clear write

  delay(10);
  * AT91C_PIOC_SODR = (1 << 15);
  * AT91C_PIOC_SODR = (1 << 17);
  * AT91C_PIOC_SODR = (1 << 12);
  * AT91C_PIOC_ODR = (0xFF << 2);
}


void Init_Display(void) {
  //*AT91C_PIOD_OER = (1);
  //*AT91C_PIOC_OER = (15 << 14);
  * AT91C_PIOD_CODR = (1); //Clear Reset display
  delay(100); //Make a Delay
  * AT91C_PIOD_SODR = (1); //Set Reset display

  Write_Data_2_Display(0x00);
  Write_Data_2_Display(0x00);
  Write_Command_2_Display(0x40); //Set text home address
  Write_Data_2_Display(0x00);
  Write_Data_2_Display(0x40);
  Write_Command_2_Display(0x42); //Set graphic home address
  Write_Data_2_Display(0x1e);
  Write_Data_2_Display(0x00);
  Write_Command_2_Display(0x41); // Set text area
  Write_Data_2_Display(0x1e);
  Write_Data_2_Display(0x00);
  Write_Command_2_Display(0x43); // Set graphic area
  Write_Data_2_Display(0x00);
  Write_Data_2_Display(0x00);
  Write_Command_2_Display(0x80); // text mode
  Write_Data_2_Display(0x00);
  Write_Data_2_Display(0x00);
  Write_Command_2_Display(0x94); // Text on graphic off
}

void displayClear() {
  Write_Data_2_Display(0x0);
  Write_Data_2_Display(0x0);
  Write_Command_2_Display(0x24);

  for (int i = 0; i < (16 * 30); i++) {
    Write_Data_2_Display(0x0);
    Write_Command_2_Display(0xC0);
  }
  Write_Data_2_Display(0x0);
  Write_Data_2_Display(0x0);
  Write_Command_2_Display(0x24);
}

int find(char c) {
  char alphabet[] = {" !'#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[�]^_�abcdefghijklmnopqrstuvwxyz{|}~"};
  for(int i=0; i< (sizeof(alphabet)/sizeof(alphabet[0])); i++){
    if(alphabet[i] == c) return i;
  }
  return -1;
}

void key_pressed(int button) {
  if (button == 1) {
    Write_Data_2_Display(0x11);
    Write_Command_2_Display(0xC0);
  } else if (button == 2) {
    Write_Data_2_Display(0x12);
    Write_Command_2_Display(0xC0);
  } else if (button == 3) {
    Write_Data_2_Display(0x13);
    Write_Command_2_Display(0xC0);
  } else if (button == 4) {
    Write_Data_2_Display(0x14);
    Write_Command_2_Display(0xC0);
  } else if (button == 5) {
    Write_Data_2_Display(0x15);
    Write_Command_2_Display(0xC0);
  } else if (button == 6) {
    Write_Data_2_Display(0x16);
    Write_Command_2_Display(0xC0);
  } else if (button == 7) {
    Write_Data_2_Display(0x17);
    Write_Command_2_Display(0xC0);
  } else if (button == 8) {
    Write_Data_2_Display(0x18);
    Write_Command_2_Display(0xC0);
  } else if (button == 9) {
    Write_Data_2_Display(0x19);
    Write_Command_2_Display(0xC0);
  } else if (button == 10) {
    Write_Data_2_Display(0x0A);
    Write_Command_2_Display(0xC0);
  } else if (button == 11) {
    Write_Data_2_Display(0x10);
    Write_Command_2_Display(0xC0);
  } else if (button == 12) {
    Write_Data_2_Display(0x03);
    Write_Command_2_Display(0xC0);
  }
}

void delay(int value) {
  int i;
  for (i = 0; i < value; i++) {
    asm("nop");
  }
}

void main() {
  SystemInit();
  config();
  Init_Display();
  displayClear();
  while (1) {
    int button = func();
    if (button != 0) {
      displayClear();
      key_pressed(button);
    }


    delay(10000);
  }
}