#include "at91sam3x8.h"
#include "system_sam3x.h"
#include "stdio.h"

#define AT91_PIOD_DIFSR (AT91_CAST(AT91_REG *) 0x400E1484)
#define AT91C_PIOD_AIMER (AT91_CAST(AT91_REG*) 0x400E14B0)
#define AT91C_PIOD_SCDR (AT91_CAST(AT91_REG *)0x400E148C)

