#include "at91sam3x8.h"
#include "system_sam3x.h"
#define AT91_PIOD_AIMER (AT91_CAST(AT91_REG *) 0x400E14B0)
#define AT91_PIOD_DIFSR (AT91_CAST(AT91_REG *) 0x400E1484)
#define AT91_PIOD_SCDR (AT91_CAST(AT91_REG *) 0x400E148C)


unsigned int time = 0, Flag=0;
unsigned int button = 0;
unsigned int count = 0;
unsigned int interrupt = 0;

void SysTick_Handler();
void config();
int ReadButton();
void Set_Led();
void PIOD_Handler();


void main()
{
  SystemInit();
  SysTick_Config((SystemCoreClock*0.001));
  config();
  while(1){
    /*ReadButton();
    if(interrupt % 2 == 0){
      Set_Led(Flag);
    }else
      Set_Led(0);*/
    *AT91C_PIOD_SODR = (1<<3);
  }
}

// ReadButton returns integer based on pressed or not | 1 if pressed | 0 if not
int ReadButton(){
  if((*AT91C_PIOD_PDSR & (1 << 1)) == (1 << 1))
    return 0;
  else {

    return 1;
  }
}


// Setting the LED on/off
void Set_Led(unsigned int nLed){
  if(nLed == 1){
    *AT91C_PIOD_SODR = (1<<3);
  }
  else if(nLed == 0){
    *AT91C_PIOD_CODR = (1<<3);
  }    
}

// Clears History adds +1 to interruptions integer
void PIOD_Handler(){
  if((( *AT91C_PIOD_ISR & (1<<1)) == (1<<1)))
    interrupt++;
}
    
// All the settings are read through at the start prefirably
void config(){
  *AT91C_PMC_PCER = (1<<14);
  *AT91C_PIOD_PER = (5<<1); //0xA
  *AT91C_PIOD_OER = (1<<3);
  *AT91C_PIOD_ODR = (1<<1);
  *AT91C_PIOD_PPUDR = (5<<1); // 0xA
  *AT91_PIOD_AIMER = (5<<1); 
  *AT91C_PIOD_IFER = (5<<1);
  *AT91_PIOD_DIFSR = (5<<1); // Bounce
  *AT91_PIOD_SCDR = 10;
  NVIC_ClearPendingIRQ(PIOD_IRQn); // Clearing Interrupt REG
  NVIC_SetPriority(PIOD_IRQn, 1); // Pending Interrupt REG
  NVIC_EnableIRQ(PIOD_IRQn); //Enable Interrupt REG
  *AT91C_PIOD_IER = (1<<1); // Enable Controller Interrupt REG
   
}

// Gives us a variable for time 
void SysTick_Handler(){
  time++;
   if(time % 1000 == 0){
      if(Flag==0)
          Flag =1;
      else 
      Flag =0;
    }
}
