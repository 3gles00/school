#ifndef _TEMP_H
#define _TEMP_H

void statusReset();
void initPuls();
void tempInit(void);
void measurement();
void TC0_Handler();
void Systick_Temp();
void tempStart();
void Temp_display(char string []);
void temp_main();

#endif

