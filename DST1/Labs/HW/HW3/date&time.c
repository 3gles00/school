#include "at91sam3x8.h"
#include "system_sam3x.h"
#include "stdio.h"
#include "Display.h"
#include "keypad.h"
#include "date&time.h"

void timeMenu (){
  setCursor(0);
  char start [] = {"Please select time and date"};
  int x = 0xC0;
  for(int i = 0; i < (sizeof(start)/sizeof(start[0])); i++){
    Write_Data_2_Display(find(start[i]));
    Write_Command_2_Display(x);
  }
  setCursor(1);
  char time [] = {"1 = time"};
  for(int i = 0; i < (sizeof(time)/sizeof(time[0])); i++){
    Write_Data_2_Display(find(time[i]));
    Write_Command_2_Display(x);
  }
  setCursor(2);
  char date [] = {"2 = date"};
  for(int i = 0; i < (sizeof(date)/sizeof(date[0])); i++){
    Write_Data_2_Display(find(date[i]));
    Write_Command_2_Display(x);
  }
  
  int button = func();
  
}