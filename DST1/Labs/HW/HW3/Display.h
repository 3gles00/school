#ifndef _DISPLAY_H
#define _DISPLAY_H

void Init_Display(void);
unsigned char Read_Status_Display(void);
void Write_Command_2_Display(unsigned char Command);
void Write_Data_2_Display(unsigned char Data);
int find(char c);
void displayClear();
void resetPointer();
void setCursor(int x);

#endif