#ifndef _SERVO_H
#define _SERVO_H

void initPWM();
void movement(int degrees);
void servo_move();

#endif