#include "at91sam3x8.h"
#include "system_sam3x.h"
#include "stdio.h"
#include "Display.h"

int func() {
  int value = 0;
  * AT91C_PIOD_OER = (1 << 2); // Output Enable REG Rows
  * AT91C_PIOC_OER = (7 << 7); // Output Enable REG Columns
  * AT91C_PIOD_CODR = (1 << 2); // Clear Output Data REG Rows 
  * AT91C_PIOC_SODR = (7 << 7); // Set Ouput Disable REG Columns
  * AT91C_PIOC_ODR = (15 << 2); // Output Disable REG Columns

  for (int i = 0; i < 3; i++) {
    * AT91C_PIOC_CODR = (1 << (7 + i)); // 
    for (int j = 0; j < 4; j++) {

      if ((( * AT91C_PIOC_PDSR & (1 << (2 + j))) == 0)) {
        value = ((j * 3) + (i + 1));

      }
    }
    * AT91C_PIOC_SODR = (7 << (7 + i));
  }
  * AT91C_PIOC_ODR = (7 << 7);
  return value;

}

void keypad_display(int button) {
  if (button == 1) {
    Write_Data_2_Display(0x11);
    Write_Command_2_Display(0xC0);
  } else if (button == 2) {
    Write_Data_2_Display(0x12);
    Write_Command_2_Display(0xC0);
  } else if (button == 3) {
    Write_Data_2_Display(0x13);
    Write_Command_2_Display(0xC0);
  } else if (button == 4) {
    Write_Data_2_Display(0x14);
    Write_Command_2_Display(0xC0);
  } else if (button == 5) {
    Write_Data_2_Display(0x15);
    Write_Command_2_Display(0xC0);
  } else if (button == 6) {
    Write_Data_2_Display(0x16);
    Write_Command_2_Display(0xC0);
  } else if (button == 7) {
    Write_Data_2_Display(0x17);
    Write_Command_2_Display(0xC0);
  } else if (button == 8) {
    Write_Data_2_Display(0x18);
    Write_Command_2_Display(0xC0);
  } else if (button == 9) {
    Write_Data_2_Display(0x19);
    Write_Command_2_Display(0xC0);
  } else if (button == 10) {
    Write_Data_2_Display(0x0A);
    Write_Command_2_Display(0xC0);
  } else if (button == 11) {
    Write_Data_2_Display(0x10);
    Write_Command_2_Display(0xC0);
  } else if (button == 12) {
    Write_Data_2_Display(0x03);
    Write_Command_2_Display(0xC0);
  }
}

