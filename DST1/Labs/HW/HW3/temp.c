#include "at91sam3x8.h"
#include "system_sam3x.h"
#include "stdio.h"
#include "temp.h"
#include "Display.h"
#include "gloabals.h"
#include "keypad.h"
// #include "main.h"

void statusReset(){
  *AT91C_TC0_CCR = (1<<2);
  *AT91C_TC0_SR;
  *AT91C_TC0_IER = (1<<6);
}

void initPuls(){
   *AT91C_PIOB_OER = (1 << 25);
   *AT91C_PIOB_CODR = (1 << 25);
   while (system < 15)
   *AT91C_PIOB_SODR = (1 << 25);
   
}

void tempInit(void){
  *AT91C_PMC_PCER = (1 << 12); //Enable PIOB Pin 2
  *AT91C_PMC_PCER = (1 << 27); // Enable TC0;
  *AT91C_TC0_CMR = (2 << 16)|(1 << 18); // LDRA = 11, LDRB 1 
  
  *AT91C_PIOB_PER = (1 << 25); // pin 2, temperature
  *AT91C_PIOB_OER = (1<<25); // Enable output
  *AT91C_PIOB_PPUDR = (1<<25); // Disable pull-up
  
  
  *AT91C_TC0_CCR = (1); //Enable Clock Command
  *AT91C_TC0_CCR = (1 << 2);
  
  initPuls();
  NVIC_ClearPendingIRQ(TC0_IRQn);
  NVIC_SetPriority(TC0_IRQn, 0);
  NVIC_EnableIRQ(TC0_IRQn); 
}

void tempStart(){
  //tempInit();
  *AT91C_PIOB_OER = (1<<25);
  *AT91C_PIOB_SODR = (1<<25);
  delay(25);
  *AT91C_PIOB_CODR = (1<<25);
  delay(25);
  *AT91C_PIOB_SODR = (1<<25);
  *AT91C_PIOB_ODR = (1<<25);
  statusReset();
}

void measurement(){
  delay(25);
  *AT91C_PIOB_OER = (1<<25);
  *AT91C_PIOB_SODR = (1<<25);
  *AT91C_PIOB_CODR = (1<<25);
  *AT91C_PIOB_SODR = (1<<25);
  *AT91C_PIOB_ODR = (1<<25);
  flag = 0;
  statusReset();
}

void Temp_display(char string[]) {
  char temperature[] = {"Temperature: "};
  for(int i = 0; i < (sizeof(temperature)/sizeof(temperature[0])); i++){
    Write_Data_2_Display(find(temperature[i]));
    Write_Command_2_Display(0xC0);                        
  }
  for(int i = 0; i < 4; i++){
    Write_Data_2_Display(find(string[i]));
    Write_Command_2_Display(0xC0);                        
  }
  Write_Data_2_Display(find('C'));
  Write_Command_2_Display(0xC0);
  
}

void temp_main(){
  if(flag == 1){
    double a = (*AT91C_TC0_RA);
    double b = (*AT91C_TC0_RB);
    measurement(); 
    double temp= b-a;
    double temp1 = (double)(temp/(42*5) - 273.15);
    resetPointer();
    char temperature[10];
    sprintf(temperature, "%lf", temp1);
    if(temp1 >(-220)){
      setCursor(1);
      Temp_display(temperature);
      //printf("%lf\n", temp1);
    }
  }
}

void TC0_Handler(){
  *AT91C_TC0_IDR = (1<<6);
  flag = 1;
}