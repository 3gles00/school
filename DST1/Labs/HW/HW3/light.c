#include "at91sam3x8.h"
#include "system_sam3x.h"
#include "stdio.h"
#include "gloabals.h"
#include "Display.h"

void adc_measurement(){
  *AT91C_ADCC_CR = (1 << 1); // Start Conversion
  *AT91C_ADCC_IER  = (3<<1);
 
  /*
  NVIC_ClearPendingIRQ((IRQn_Type)37);
  NVIC_SetPriority( ((IRQn_Type)37) , 0);
  NVIC_EnableIRQ((IRQn_Type)37);*/
}

void adc_Init(){
  *AT91C_PMC_PCER1 = (1<<5);//adc
  *AT91C_ADCC_MR = (2<<8); // Set prescaler to 2  
   *AT91C_ADCC_CHER = (3 << 1); //Enables CH 1 - 2

  adc_measurement();
}

void adc_Handler(){
  if(adcRdyFlag == 1) adcRdyFlag = 0;
  else if(adcRdyFlag == 0) adcRdyFlag = 1;
  one = (*AT91C_ADCC_CDR1) & (0xFFF);
  two = (*AT91C_ADCC_CDR2) & (0xFFF);

  *AT91C_ADCC_CR = 2; // Start ADC conversion

}

void adc_display(char stringA[], char stringB[]){
  setCursor(2);
  char channalA[] = {"Channal A: "};
  int x = 0xC0;
  // printf("\nChannal A: %s\n", stringA);
  for(int i = 0; i < (sizeof(channalA)/sizeof(channalA[0])); i++){
    Write_Data_2_Display(find(channalA[i]));
    Write_Command_2_Display(x);
  }
  for(int i = 0; i < 4; i++){
    if(find(stringA[i]) != (-1)){
      Write_Data_2_Display(find(stringA[i]));
      Write_Command_2_Display(x);
    }
    /*if(find(stringA[i])  == (-1)){
      printf("Could not find %c\n", stringA[i]); // 
    }*/
  }
  Write_Data_2_Display(find('V'));
  Write_Command_2_Display(0xC0);
  
  setCursor(3);
  char channalB[] = {"Channal B: "};
  // printf("\nChannal A: %s\n", stringA);
  for(int i = 0; i < (sizeof(channalB)/sizeof(channalB[0])); i++){
    Write_Data_2_Display(find(channalB[i]));
    Write_Command_2_Display(x);
  }
  for(int i = 0; i < 4; i++){
    if(find(stringB[i]) != (-1)){
      Write_Data_2_Display(find(stringB[i]));
      Write_Command_2_Display(x);
    }
    /*if(find(stringA[i])  == (-1)){
      printf("Could not find %c\n", stringA[i]); // 
    }*/
  }
  Write_Data_2_Display(find('V'));
  Write_Command_2_Display(0xC0);
  
}

void adc_main(){
  adc_Handler();
  if (adcRdyFlag == 1)
    {
      adcRdyFlag = 0;

      double a = ((double)one) / 4095 * 3.3f;
      double b = ((double)two) / 4095 * 3.3f;
      resetPointer();
     char A[10];
      char B[10];
      sprintf(A, "%lf", a);
      sprintf(B, "%lf", b);
      adc_display(A, B); 
     // printf("light one %lf\n", a);
    //  printf("light two %lf\n", b);

      
      delay(500);
    }
}


