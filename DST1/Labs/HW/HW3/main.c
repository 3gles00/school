#include "at91sam3x8.h"
#include "system_sam3x.h"
#include "stdio.h"
#include "Display.h"
#include "keypad.h"
#include "temp.h"
#include "gloabals.h"
#include "lights.h"
#include "servo.h"

#define AT91_PIOD_DIFSR (AT91_CAST(AT91_REG * ) 0x400E1484)
#define AT91C_PIOD_AIMER (AT91_CAST(AT91_REG * ) 0x400E14B0)
#define AT91C_PIOD_SCDR (AT91_CAST(AT91_REG * ) 0x400E148C)


void delay(int a);
void config();
void main();
void SystTick_Handler();



void config() {
  * AT91C_PMC_PCER = (3 << 13);

  * AT91C_PIOC_PER = (0x3FC);
  * AT91C_PIOD_PER = (5); // Enable pin 27 and 25
  * AT91C_PIOC_PER = (0xFF000); // enable pin 44 - 51
  * AT91C_PIOD_OER = (5 << 0); //configure pin 25 and 27 as output
  * AT91C_PIOC_OER = (0xFF000); // Enables Data Bus 

}

  void main() {
  SystemInit();
  SysTick_Config(SystemCoreClock/1000);
  config(); 
  tempInit();
  tempStart();
  initPWM();
  Init_Display(); 
  adc_Init();  
  displayClear();
  while (1) {
    temp_main();
    adc_main();
    servo_move();
    
    
    
  }    
}
    
void SysTick_Handler(){
  system++;
  if(system % 10 == 0){
    TC0_Handler();
  }
}