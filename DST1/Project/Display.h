#ifndef _DISPLAY_H
#define _DISPLAY_H

unsigned char Read_Status_Display(void);
void Write_Command_2_Display(unsigned char Command);
void Write_Data_2_Display(unsigned char Data);
void Init_Display(void);
int find(char c);
void Print_String_2_Display(char string[], int size);
int pointerPosition();
void setRow();
void setRowNr(int y);
void setColumn(int x);
void resetPointer();
void resetCursor();
void displayClear();
void fillDisplay();

#endif