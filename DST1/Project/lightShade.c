#include "at91sam3x8.h"
#include "system_sam3x.h"
#include "stdio.h"
#include "Display.h"
#include "globals.h"
#include "lightSensor.h"
#include "servo.h"
#include "LED.h"
#include "keypad.h"

double degree;
int sunHours = 0;

void measuramentDegree(){
    double maxDifference = 1.25;
    // + 90 since our 0 should be 
    // 180 - degrees since Voltage goes up with drkness
    degree = 180.0 - (90.0 + (eastLight - westLight)/(maxDifference/180.0));
    if(degree > 180.0) degree = 180.0;
    else if (degree < 0) degree = 0;
    movement(degree);
}

void artificalSun(){
    if(westLight < 2 && eastLight < 2)
      if(fastModeFlag == 1) lightGainedSeconds += 30*60;
      else lightGainedSeconds++;
      ledOff();
    if((westLight > 2 && eastLight > 2) && lightGainedSeconds < 16*3600){
      if(fastModeFlag == 1) lightGainedSeconds += 30*60;
      else lightGainedSeconds++;
      ledOn();
    }
    else ledOff();
}

int getDegrees(){
    return (int) degree;
}

void lightShadePrint(){
    char degree[17];
    sprintf(degree, "Degrees: %d%d%d*", 
        (int) getDegrees()/100,
        (int) (getDegrees() % 100)/10,
        (int) getDegrees()%10);
    char west[20];
        sprintf(west, "West Shade: %d,%d V",
        (int)(westLight *10) / 10,
        (int)(westLight *10) % 10);
    char east[20];
        sprintf(east, "East Shade: %d,%d V", 
        (int)(eastLight * 10)/10,
        (int)(eastLight * 10) %10);
    Print_String_2_Display(degree, 14);
    setRow();
    Print_String_2_Display(west, 18);
    setRow();
    Print_String_2_Display(east, 18);
}

void lightShadeMenu(){
    displayClear();
    setRowNr(3);
    Print_String_2_Display("1: Reread Values   *: Return", 29);
    setRow();
    setRow();
    lightShadePrint();
    fillDisplay();
    switch(validKeyPress()){
        case 1:
            lightShadeMenu();
        break;
        case 2:
            lightShadeMenu();
        break;
        case 3:
            lightShadeMenu();
        break;
        case 4:
            lightShadeMenu();
        break;
        case 5:
            lightShadeMenu();
        break;
        case 6:
            lightShadeMenu();
        break;
        case 7:
            lightShadeMenu();
        break;
        case 8:
            lightShadeMenu();
        break;
        case 9:
            lightShadeMenu();
        break;
        case 10:
        break;
        case 11:
            lightShadeMenu();
        break;
        case 12:
            lightShadeMenu();
        break;
    }
} 