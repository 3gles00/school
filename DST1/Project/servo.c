#include "at91sam3x8.h"
#include "system_sam3x.h"
#include "stdio.h"
#include "keypad.h"
#include "globals.h"
#include "Display.h"
#include "lightShade.h"

void movement(double degrees);

void initPWM(){
  *AT91C_PMC_PCER = (1<<12); //enable clock pioB;
  *AT91C_PMC_PCER1 = (1<<4);//PWM enabling;
  *AT91C_PIOB_PDR = (1<<17); // turn off the pin according to the analog;
  *AT91C_PIOB_ABMR = (1<<17); // activate peripheral B to control REG_PIOB_ABSR;
  *AT91C_PWMC_ENA = (1<<1);
  *AT91C_PWMC_CH1_CMR = 5;
  *AT91C_PWMC_CH1_CPRDR = (52500); // Our Master Clock at 84 000'000 (20ms);
  *AT91C_PWMC_CH1_CDTYR = (2625); //(1ms);
  
  movement(0);
  delay(100);
  movement(90);
  delay(100);
}

//0 grader ar 0,7ms och 180 grader ar 2,3ms
void movement(double degrees){
  degrees = degrees / 10 ; //decreasing to accurate degrees
  (*AT91C_PWMC_CH1_CDTYR) =  (int)(degrees*233 + 1838);
}

void servo_move(){
  measuramentDegree();
 // movement(servoDegrees);
}