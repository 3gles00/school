#ifndef _DATE_H
#define _DATE_H

int getSeconds(int time);
int getMinutes(int time);
int getHours(int time);
int getDays(int date);
int getMonths(int date);
int getYears(int date);
void timeToString();
void dateToString();
void update();
void timeOverflow();
void printCurrentTime();
void timeIncrement();
int checkValidDate(int time);
int checkValidTime(int time);
void timeConfig();
void dateConfig();
void setDate();
void setTime();
void timeInit();
void menuTime();

#endif