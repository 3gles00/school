#ifndef _TEMP_H
#define _TEMP_H

void statusReset();
void initPuls();
void tempInit();
void tempStart();
void measurement();
void tempDisplay();
void temp_main();
void TC0_Handler();
void tempMenu();

#endif

