#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "globals.h"
#include "date.h"
#include "Display.h"
#include "keypad.h"
#include "temp.h"

int page = 1;
double totalDayTemp = 0;

struct LinkedListDay *LATEST_DAY = NULL;

/**
 * Structur for the LinkedList nodes
*/
struct LinkedListNode{
    unsigned int time; // Counted in seconds
    int tempData;
    struct LinkedListNode *next;
};

/**
 * Structur for the LinkedList nodes representing days recorded
*/
struct LinkedListDay{
    unsigned int size, date;
    int avgTempData;
    struct LinkedListNode *firstNode;
    struct LinkedListNode *maxTemp;
    struct LinkedListNode *minTemp;
    struct LinkedListDay *nextDay;
};

/**
 * Compares to given nodes for which is larger and which is smaller or equal
*/
int compare(struct LinkedListNode *el1, struct LinkedListNode *el2){
    if((el1 -> tempData) > (el2 -> tempData))
        return 1;
    if((el1 -> tempData) < (el2 -> tempData))
        return -1;
    return 0;
}

void getMax(struct LinkedListDay *Day){
    if(Day -> size == 1){
        Day -> maxTemp -> tempData = Day -> firstNode -> tempData;
        Day -> maxTemp -> time = Day -> firstNode -> time;
    }
    else if(compare(Day->firstNode, Day->maxTemp) == 1){
        Day -> maxTemp -> tempData = Day -> firstNode -> tempData;
        Day -> maxTemp -> time = Day -> firstNode -> time;
    }
        
}

void getMin(struct LinkedListDay *Day){
    if(Day -> size == 1){
        Day -> minTemp -> tempData = Day -> firstNode -> tempData;
        Day -> minTemp -> time = Day -> firstNode -> time;
    }
    else if(compare(Day->firstNode, Day->minTemp) == -1){
        Day -> minTemp -> tempData = Day -> firstNode -> tempData;
        Day -> minTemp -> time = Day -> firstNode -> time;
    }    
}

/**
 * Sets a new node in the first place of the list
 * Updates the avgTemp
*/
struct LinkedListNode * insertFirst(struct LinkedListNode *el, struct LinkedListNode *first){
    (el -> next) = first;
    first = el;
    return first;
}

/**
 * Removes the last node in the list
*/
void removeLastNode(struct LinkedListDay *day) {
    struct LinkedListNode *temp = day -> firstNode, *prev;
    if(temp != NULL){
        while((temp->next) != NULL){
            prev = temp;
            temp = temp -> next;
        }
        prev -> next = NULL;
        if(&(day -> firstNode) == &temp)
            day -> firstNode = NULL;
        free(temp);
    }
}

/**
 * removes the last Day in the list with all its nodes
*/
void removeLastDay(struct LinkedListDay *firstDay){
    struct LinkedListDay *remove = firstDay, *prevDay;
    while(remove -> nextDay != NULL){
        prevDay = remove;
        remove = remove -> nextDay;
    }
    free(remove -> maxTemp);
    free(remove -> minTemp);
    free(remove);
    prevDay -> nextDay = NULL;
}

struct LinkedListDay * insertDay(struct LinkedListDay *first, struct LinkedListDay *newDay){
    newDay -> nextDay = first;
    return newDay;
}

struct LinkedListNode * purgeList(struct LinkedListNode *first){
    struct LinkedListNode * temp;
    if(first != NULL){
        while(first != NULL){
            temp = first -> next;
            free(first);
            first = temp;
        }
    }
    return NULL;
}

/**
 * Makes a new list for each day new day based on the REG_TIME of the system
 * Will remove the last day if the total amount of days is over 7
*/
void newDay(){
    lightGainedSeconds = 0;
    if(dayCounter != 0)
        LATEST_DAY -> firstNode = purgeList(LATEST_DAY -> firstNode);
    if(dayCounter == 7){
        removeLastDay(LATEST_DAY);
        dayCounter--;
    }
    struct LinkedListDay *newDay = malloc(sizeof(struct LinkedListDay));
    dayCounter++;
    newDay -> size = 0;
    newDay -> date = REG_DATE;
    newDay -> firstNode = NULL;
    newDay -> maxTemp = malloc(sizeof(struct LinkedListNode));
    newDay -> minTemp = malloc(sizeof(struct LinkedListNode));
    newDay -> maxTemp -> tempData = -2745;
    newDay -> minTemp -> tempData = 2000;
    totalDayTemp = 0;
    LATEST_DAY = insertDay(LATEST_DAY, newDay);
}

/**
 * Takes values from the current REG_TIME and inserts them in a new list item
 * Will remove the last node if there is not enough space
*/
void insertData(){
    struct LinkedListNode *el = (struct LinkedListNode *) malloc(sizeof(struct LinkedListNode));
    if(el == NULL){
        removeLastNode(LATEST_DAY);
        el = (struct LinkedListNode * ) malloc(sizeof(struct LinkedListNode));
    }
    el -> time = REG_TIME;
    el -> tempData = (int) (temperature * 10 / 1);
    LATEST_DAY -> firstNode = insertFirst(el, (LATEST_DAY -> firstNode));
    getMax(LATEST_DAY);
    getMin(LATEST_DAY);
    totalDayTemp += el -> tempData;
    LATEST_DAY -> size++;
    LATEST_DAY -> avgTempData = (int) (totalDayTemp / LATEST_DAY -> size); 
}

/**
 * Printes the Node with the REG_TIME and REG_DATE stamp and tempereture
*/
void printNode(struct LinkedListNode *el){
    char stamp[23];
    sprintf(stamp, "%d%d:%d%d:%d%d %d,%d C",
        getHours(el -> time)/10, 
        getHours(el -> time)%10,
        getMinutes(el -> time)/10,
        getMinutes(el -> time)%10,
        getSeconds(el -> time)/10,
        getSeconds(el -> time)%10,
        el -> tempData / 10,
        el -> tempData % 10);
    Print_String_2_Display(stamp, 15);
}

// List Display prints the max, min and avg temperature to the display
void printDayStats(struct LinkedListDay *day){
    char date[40];
    sprintf(date, "Date %d%d/%d%d/%d%d%d%d Size %d        ",
        getDays(day -> date)/10,
        getDays(day -> date)%10,
        getMonths(day->date)/10,
        getMonths(day -> date)%10,
        getYears(day -> date)/1000,
        (getYears(day -> date)%1000)/100,
        (getYears(day -> date)%100)/10,
        getYears(day -> date)%10,
        day -> size);
    Print_String_2_Display(date, 30);
    setRow();
    char avg[12];
    sprintf(avg, "Avg %d,%d C",
        day -> avgTempData / 10,
        day -> avgTempData % 10);
    Print_String_2_Display(avg, sizeof(avg)/sizeof(avg[0]));
    setRow();
    Print_String_2_Display("Max ", 4);
    printNode(day -> maxTemp);
    setRow();
    Print_String_2_Display("Min ", 4);
    printNode(day -> minTemp);
}

/**
 * Function for if we press to show stats before 1 min has passed
*/
void nullStats(struct LinkedListDay *day){
    char date[40];
    sprintf(date, "Date %d%d/%d%d/%d%d%d%d Size %d        ",
        getDays(day -> date)/10,
        getDays(day -> date)%10,
        getMonths(day->date)/10,
        getMonths(day -> date)%10,
        getYears(day -> date)/1000,
        (getYears(day -> date)%1000)/100,
        (getYears(day -> date)%100)/10,
        getYears(day -> date)%10,
          day -> size);
    Print_String_2_Display(date, 30);
    setRow();
    setRow();
    Print_String_2_Display("Need To gather more Data", 25);
}

/**
 * printPage will find the correct Day to print
 * Starts with the latest Day
*/
void printPage(int page){
    int i = 1;
    struct LinkedListDay *day = LATEST_DAY;
    while(i != page){
        day = day -> nextDay;
        i++;
    }
    setRow();
    setRow();
    if(day -> minTemp -> tempData == 2000)
        nullStats(day);
    else printDayStats(day);
}

/**
 * Recursive method exits with the * also known as the value 10
 */
void recordingVault(){
    displayClear();
    setRowNr(3);
    if(page < 1){
        page = 1;
    }
    if(page > dayCounter){
        page = dayCounter;
    }
    setRow();
    setRow();
    Print_String_2_Display("4: Prev 6: Next *: Return", 26);
    printPage(page);
    fillDisplay();
    switch(validKeyPress()){
        case 4:
            if(page != 1)
                page--;
            recordingVault();
            break;
        case 6:
            if(!(page >= dayCounter))
                page++;
            recordingVault();
            break;
        case 10:
            break;
    }
}
