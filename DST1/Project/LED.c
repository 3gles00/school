#include "at91sam3x8.h"
#include "system_sam3x.h"
#define AT91_PIOD_AIMER (AT91_CAST(AT91_REG *) 0x400E14B0)
#define AT91_PIOD_DIFSR (AT91_CAST(AT91_REG *) 0x400E1484)
#define AT91_PIOD_SCDR (AT91_CAST(AT91_REG *) 0x400E148C)

short nLed = 0;

void LEDConfig(){
  *AT91C_PIOD_PER = (1<<3); // Enable PIO control for PIN 28
  *AT91C_PIOD_OER = (1<<3); // Enable Pin Output 
  *AT91C_PIOD_ODR = (1<<1); // Disable the pullup resistor
  *AT91C_PIOD_PPUDR = (1<<3); // Disable the Pullup resistor
  *AT91C_PIOD_CODR = (1<<3); // Setting the LED to 0
  *AT91C_PIOD_SODR = (1<<3); // Setting the LED to 1 
  *AT91C_PIOD_CODR = (1<<3); // Clearing the LED preparing for action
}

void ledOff(){
  *AT91C_PIOD_CODR = (1<<3); // Clear the Pin to disable led
  nLed = 1;
}

void ledOn(){
  *AT91C_PIOD_CODR = (1<<3); // Clearing from previous actions
  *AT91C_PIOD_SODR = (1<<3); // Seting the Pin to on
  nLed = 1; 
}

// Setting the LED on/off
void set_Led(){
  if(nLed == 1){
    *AT91C_PIOD_SODR = (1<<3);
    nLed = 0;
  }
  else if(nLed == 0){
    *AT91C_PIOD_CODR = (1<<3);
    nLed = 1;
  }    
}

void alarm(){
  set_Led();
}