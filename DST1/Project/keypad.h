#ifndef _KEYPAD_H
#define _KEYPAD_H

int keypadPress();
int isKeyPressed();
void keyReleased();
int validKeyPress();
void keypad_display(int button);


#endif