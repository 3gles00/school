#include "at91sam3x8.h"
#include "system_sam3x.h"
#include "stdio.h"
#include "Display.h"
#include "keypad.h"
#include "globals.h"
#include "math.h"
#include "LinkedList.h"
#include "LED.h"
#include "temp.h"
#include "lightSensor.h"
#include "lightShade.h"
#include "servo.h"

void setDate();
void setTime();

int bitExtracted(int numb, int k, int p){
  return (((1 << k) - 1 & (numb >> (p - 1))));
}

int getSeconds(int time){
  return bitExtracted(time, 8, 1);
}

int getMinutes(int time){
  return bitExtracted(time, 8, 9);
}

int getHours(int time){
  return bitExtracted(time, 16, 17);
}

int getDays(int date){
  return bitExtracted(date, 8, 1);
}

int getMonths(int date){
  return bitExtracted(date, 8, 9);
}

int getYears(int date){
  return bitExtracted(date, 16, 17);
}

/**
 * Returns the time to char array format set in our global
 * char array timeString
*/
void timeToString(){
  //seconds bits
  int s1 = getSeconds(REG_TIME)/10;
  int s2 = getSeconds(REG_TIME)%10;
  //minutes bits
  int m1 = getMinutes(REG_TIME)/10;
  int m2 = getMinutes(REG_TIME)%10;
  //hours bits
  int h1 = getHours(REG_TIME)/10;
  int h2 = getHours(REG_TIME)%10;
  
  sprintf(timeString,"%d%d:%d%d:%d%d", h1,h2,m1,m2,s1,s2);
  //printf("%s ", timeString);
}

/**
 * Returns the date to char array format set in our global
 * char array dateString
*/
void dateToString(){
  int d1 = getDays(REG_DATE)/10;
  int d2 = getDays(REG_DATE)%10;
  //minutes bits
  int mo1 = getMonths(REG_DATE)/10;
  int mo2 = getMonths(REG_DATE)%10;
  //hours bits
  int y1 = getYears(REG_DATE)%10;
  int y2 = (getYears(REG_DATE)/10)%10;
  int y3 = (getYears(REG_DATE)/100)%10;
  int y4 = (getYears(REG_DATE)/1000)%10;

  sprintf(dateString,"%d%d/%d%d/%d%d%d%d", d1,d2,mo1,mo2,y4,y3,y2,y1);
  //printf("%s ", dateString);
}

/**
 * Function updates the time
*/
void update(){
  dateToString();
  timeToString();
}

/**
 * Checks for overflow of time
*/
void timeOverflow(){
  int maxDaysMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
  int seconds = getSeconds(REG_TIME);
  int minutes = getMinutes(REG_TIME);
  int hours = getHours(REG_TIME);
  int days = getDays(REG_DATE);
  int months = getMonths(REG_DATE);
  int years = getYears(REG_DATE);
  int temp = 0;
  if(years %4 == 0) {
    maxDaysMonth[1] = 29;
  }
  if(seconds > 59){
    minutes += (seconds/60);
    seconds = (seconds%60);
    if(minutes > 59){
      hours += (minutes/60);
      minutes = (minutes%60);
      if(hours > 23){
        days += (hours / 24);
        hours = (hours%24);
        if(days > maxDaysMonth[months-1]){
          months += (days / maxDaysMonth[months-1]);
          days = (days % (maxDaysMonth[months-1]));
          if(months > 12) {
            years += (months / 12);
            months = (months%12);
          }
        }
        if(years > 9999) years = 0;
        temp |= (years << 16);
        temp |= (months << 8);
        temp |= (days << 0);
        REG_DATE = temp;
        newDay();
      }
    }
    temp = 0;
    temp |= (hours << 16);
    temp |= (minutes << 8);
    temp |= (seconds << 0);
    REG_TIME = temp;
  }
}

/**
 * Function prints the first line on top of the display with the current temperature
*/
void printCurrentTime(){
  if(menuFlag == 1 && printTimeFlag == 1){
    update();
    setRowNr(0);
    Print_String_2_Display(timeString, 9);
    setColumn(10);
    tempDisplay();
    setColumn(18);
    Print_String_2_Display(dateString, 11);
    setRowNr(1);
    Print_String_2_Display("-----------------------------", 30);
  }
}

void secIncrement(){
  int sec = getSeconds(REG_TIME)+1;
  int temp = REG_TIME;
  temp = (temp >> 8);
  temp = (temp << 8);
  temp |= sec;
  REG_TIME = temp;
}

void minIncremenet(){
  int temp = REG_TIME;
  int sec = getSeconds(temp)+60;
  temp = (temp >> 8);
  temp = (temp << 8);
  temp |= sec;
  REG_TIME = temp;
}

/**
 * Function Increments the REG_TIME
*/
void timeIncrement(){
  if(menuFlag != 0){
    if(fastModeFlag == 1){
      minIncremenet();
    }
    else {
      secIncrement();
    }
    timeOverflow();
  }
}

/**
 * Checks if given REG_DATE is valid returns 1 or 0
 */
int checkValidDate(int date){
  int daysInMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
  int years = getYears(date);
  int months = getMonths(date);
  int days = getDays(date); 
  if(years % 4 == 0) daysInMonth[1] = 29;
  if((months > 0 && months < 13) && (days > 0 && days < daysInMonth[months] && years < 10000)){
    return 1;
  }
  return 0;
}

/**
 * Checks if the REG_TIME is valid returns based 1 or 0
*/
int checkValidTime(int time){
  if(getSeconds(time) < 60)
    if(getMinutes(time) < 60)
      if(getHours(time) < 24)
        return 1;
  return 0;
}

/**
 * Function handles the manual implementation of Time
*/
void timeConfig(){
  displayClear();
  Print_String_2_Display("Set Time: HH:MM:SS", 19);
  setTime();
  displayClear();
}

/**
 * Function handles the manual implementation of Date
*/
void dateConfig(){
  displayClear();
  Print_String_2_Display( "Set Date: DD/MM/YYYY", 21);
  setDate();
  newDay();
  displayClear();
}

/**
 * Function sets the Date for our global variabel
*/
void setDate(){
  int date[8];
  resetCursor();
  //days
  for(int i = 10; i < 12; i++){
    while(isKeyPressed() == 0);
    date[i-10] = validKeyPress();
    setColumn(i);
    keypad_display(date[i-10]);
  }
  //months
  for(int i = 13; i < 15; i++){
    while(isKeyPressed() == 0);
    date[i-11] = validKeyPress();
    setColumn(i);
    keypad_display(date[i-11]);
  }
  //years
  for(int i = 16; i < 20; i++){
    while(isKeyPressed() == 0);
    date[i-12] = validKeyPress();
    setColumn(i);
    keypad_display(date[i-12]);
  }
  int days = date[0]*10+date[1];
  int months = date[2]*10+date[3];
  int years = date[4]*1000+date[5]*100+date[6]*10+date[7];
  int daysInMonth[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
  if(years % 4 == 0) daysInMonth[1] = 29;
  int daysFromMonth = 0;
  for(int i = 0; i< (months-1); i++){
    daysFromMonth += daysInMonth[i];
  }
  int temp = 0;
  temp |= (years << 16);
  temp |= (months << 8);
  temp |= (days << 0);
  dateToString();
  if(checkValidDate(temp) == 0){
    dateConfig();
  }
  else REG_DATE = temp;
}

/**
 * Function sets the REG_TIME as a global variabel
*/
void setTime(){
  int time[6];
  resetCursor();
  //hours
  for(int i = 10; i < 12; i++){
    while(isKeyPressed() == 0);
    time[i-10] = validKeyPress();
    setColumn(i);
    keypad_display(time[i-10]);
  }

  //minutes
  for(int i = 13; i < 15; i++){
    while(isKeyPressed() == 0);
    time[i-11] = validKeyPress();
    setColumn(i);
    keypad_display(time[i-11]);
  }  

  //seconds
  for(int i = 16; i < 18; i++){
    while(isKeyPressed() == 0);
    time[i-12] = validKeyPress();
    setColumn(i);
    keypad_display(time[i-12]);
  }

  int hours = time[0]*10+time[1];
  int minutes = time[2]*10+time[3];
  int seconds = time[4]*10+time[5];

  int temp = 0;
  temp |= (hours << 16);
  temp |= (minutes << 8);
  temp |= (seconds << 0);
  timeToString();
  if(checkValidTime(temp) == 0)
    timeConfig();
  else REG_TIME = temp;
}


/**
 * Function handles the menu for setting REG_TIME and REG_DATE.
 * Will delete all prior nodes if change is made to REG_TIME and REG_DATE.
*/
void timeInit(){
  dateConfig();
  timeConfig();
  SYSTEM_TICK = 0;
  timeTick = 0;
  update();
}

/**
 * menuTime lets you change the invdividual REG_TIME or REG_DATE or both.
*/
void menuTime(){
  displayClear();
  setRowNr(3);
  Print_String_2_Display("Please select the following:", 29);
  setRow();
  setRow();
  Print_String_2_Display("1: Set Time", 12);
  setRow();
  Print_String_2_Display("2: Set Date", 12);
  setRow();
  Print_String_2_Display("3: Set Both", 12);
  setRow();
  Print_String_2_Display("*: Return", 10);
  fillDisplay();
  switch (validKeyPress()){
    case 1:
      menuFlag = 0;
      timeConfig();
      menuFlag = 1;
      menuTime();
      break;
    case 2:
      menuFlag = 0;
      dateConfig();
      newDay();
      menuFlag = 1;
      menuTime();
      break;
    case 3:
      menuFlag = 0;
      timeInit();
      menuFlag = 1;
      menuTime();
      break;
  }
}