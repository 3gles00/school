#include "at91sam3x8.h"
#include "system_sam3x.h"
#include "stdio.h"
#include "Display.h"
#include "keypad.h"
#include "temp.h"
#include "globals.h"
#include "lightSensor.h"
#include "servo.h"
#include "date.h"
#include "LinkedList.h"
#include "LED.h"
#include "lightShade.h"

#define AT91_PIOD_DIFSR (AT91_CAST(AT91_REG * ) 0x400E1484)
#define AT91C_PIOD_AIMER (AT91_CAST(AT91_REG * ) 0x400E14B0)
#define AT91C_PIOD_SCDR (AT91_CAST(AT91_REG * ) 0x400E148C)

void config();
void main();

void config() {
  SysTick_Config((SystemCoreClock/1000));
  * AT91C_PMC_PCER = (3 << 13);
  * AT91C_PIOC_PER = (0x3FC);
  * AT91C_PIOD_PER = (5); // Enable pin 27 and 25
  * AT91C_PIOC_PER = (0xFF000); // enable pin 44 - 51
  * AT91C_PIOD_OER = (5 << 0); //configure pin 25 and 27 as output
  * AT91C_PIOC_OER = (0xFF000); // Enables Data Bus 
  tempInit();
  tempStart();
  initPWM();
  Init_Display(); 
  adc_Init();  
  displayClear();
  LEDConfig();
}

void menuSelect(){
  displayClear();
  if(menuFlag != 1){
    timeInit();
    menuFlag = 1;
    printTimeFlag = 1;
  }
  setRowNr(3);
  Print_String_2_Display("Please select the following:", 29);
  setRow();
  setRow();
  Print_String_2_Display("1: Date and Time config", 24);
  setRow();
  Print_String_2_Display("2: Temperature recording ", 26);
  setRow();
  Print_String_2_Display("3: Enable shadow realm ", 24);
  setRow();
  Print_String_2_Display("4: Fast Mode", 13);
  fillDisplay();
  switch (validKeyPress()){
    case 1:
      menuTime();
      break;
    case 2: 
      tempMenu();
      break;
    case 3:
      lightShadeMenu();
      break;
    case 4:
      fastModeMenu();
      break;
  }
}

void main() {
  SystemInit();
  config();
  while(1){
   menuSelect(); 
  }    
}
