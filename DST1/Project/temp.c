#include "at91sam3x8.h"
#include "system_sam3x.h"
#include "stdio.h"
#include "temp.h"
#include "Display.h"
#include "globals.h"
#include "keypad.h"
#include "LinkedList.h"

void statusReset(){
  *AT91C_TC0_CCR = (1<<2);
  *AT91C_TC0_SR;
  *AT91C_TC0_IER = (1<<6);
}

void initPuls(){
  *AT91C_PIOB_OER = (1 << 25);
  *AT91C_PIOB_CODR = (1 << 25);
  while (SYSTEM_TICK < 15)
    *AT91C_PIOB_SODR = (1 << 25);
}

void tempInit(){
  *AT91C_PMC_PCER = (1 << 12); //Enable PIOB Pin 2
  *AT91C_PMC_PCER = (1 << 27); // Enable TC0;
  *AT91C_TC0_CMR = (2 << 16)|(1 << 18); // LDRA = 11, LDRB 1 
  *AT91C_PIOB_PER = (1 << 25); // pin 2, temperature
  *AT91C_PIOB_OER = (1<<25); // Enable output
  *AT91C_PIOB_PPUDR = (1<<25); // Disable pull-up
  *AT91C_TC0_CCR = (1); //Enable Clock Command
  *AT91C_TC0_CCR = (1 << 2);
  initPuls();
  NVIC_ClearPendingIRQ(TC0_IRQn);
  NVIC_SetPriority(TC0_IRQn, 0);
  NVIC_EnableIRQ(TC0_IRQn); 
}

void tempStart(){
  //tempInit();
  *AT91C_PIOB_OER = (1<<25);
  *AT91C_PIOB_SODR = (1<<25);
  delay(25);
  *AT91C_PIOB_CODR = (1<<25);
  delay(25);
  *AT91C_PIOB_SODR = (1<<25);
  *AT91C_PIOB_ODR = (1<<25);
  statusReset();
}

void measurement(){
  delay(25);
  *AT91C_PIOB_OER = (1<<25);
  *AT91C_PIOB_SODR = (1<<25);
  *AT91C_PIOB_CODR = (1<<25);
  *AT91C_PIOB_SODR = (1<<25);
  *AT91C_PIOB_ODR = (1<<25);
  statusReset();
}

void tempDisplay() {
  char string[11];
  sprintf(string, "%lf", temperature);
  Print_String_2_Display(string, 4);
  Print_String_2_Display("C", 1);
}

/**
 * Temp Main measures the temperature and sets it to a global value
*/
void temp_main(){
  double a = (*AT91C_TC0_RA);
  double b = (*AT91C_TC0_RB);
  measurement(); 
  double temp= b-a;
  if((temp/(42*5) - 273.15) > (-250))
    temperature = (double)(temp/(42*5) - 273.15);
  
}

/**
 * Temperautre Handler
*/
void TC0_Handler(){
  *AT91C_TC0_IDR = (1<<6);
  temp_main();
}

void changeUpperLimit(){
  int temp[2];
  displayClear();
  Print_String_2_Display("Set Upper Limit: --C", 21);
  resetCursor();
  for(int i = 17; i < 19; i++){
    while(isKeyPressed() == 0);
    setColumn(i);
    temp[i-17] = validKeyPress();
    keypad_display(temp[i-17]);
  }
  upperLimit = temp[0]*10+temp[1];
  if(upperLimit < lowerLimit || upperLimit > 99) changeUpperLimit();
}

void changeLowerLimit(){
  int temp[2];
  displayClear();
  Print_String_2_Display("Set Lower Limit: --C", 21);
  resetCursor();
  for(int i = 17; i < 19; i++){
    while(isKeyPressed() == 0);
    setColumn(i);
    temp[i-17] = validKeyPress();
    keypad_display(temp[i-17]);
  }
  lowerLimit = temp[0]*10+temp[1];
  if(upperLimit < lowerLimit) changeLowerLimit();
}

void temperatureLimit(){
  displayClear();
  setRowNr(3);
  Print_String_2_Display("Current Limits are:", 20);
  char high[9];
  sprintf(high, "High: %d", upperLimit);
  setRow();
  Print_String_2_Display(high, 9);
  char low[8];
  sprintf(low, "Low: %d", lowerLimit);
  setRow();
  Print_String_2_Display(low, 8);
  setRow();
  setRow();
  Print_String_2_Display("1: Set High Limit", 18);
  setRow();
  Print_String_2_Display("2: Set Low Limit", 17);
  setRow();
  Print_String_2_Display("*: Return", 10);
  fillDisplay();
  switch (validKeyPress())
  {
  case 1:
    printTimeFlag = 0;
    changeUpperLimit();
    printTimeFlag = 1;
    temperatureLimit();
    break;
  case 2:
    printTimeFlag = 0;
    changeLowerLimit();
    printTimeFlag = 1;
    temperatureLimit();
  case 10:
    break;
  }
}

void tempMenu(){
  displayClear();
  setRowNr(3);
  Print_String_2_Display("Please select the following:", 29);
  setRow();
  setRow();
  Print_String_2_Display("1: Check Stats", 15);
  setRow();
  setRow();
  Print_String_2_Display("2: Change temperature limit", 28);
  setRow();
  setRow();
  Print_String_2_Display("*: Return Menu ", 16);
  fillDisplay();
  switch (validKeyPress()){
    case 1:
      recordingVault();
      tempMenu();
      break;
    case 2:
      temperatureLimit();
      tempMenu();
      break;
    case 10: 
      break;
  }
}