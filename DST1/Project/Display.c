#include "at91sam3x8.h"
#include "system_sam3x.h"
#include "globals.h"


unsigned int row = 0, column = 0, pointer = 0;

unsigned char Read_Status_Display(void){
  unsigned char temp;      
  *AT91C_PIOC_ODR = (0x3FC);         // set all databus pins as input
  
  *AT91C_PIOC_OER = (1 << 13); 
  *AT91C_PIOC_SODR = (1 << 13);     // pin 50 Set dir as high
  *AT91C_PIOC_OER = (1 << 12);
  *AT91C_PIOC_CODR = (1 << 12);    // pin 51 clear output
  *AT91C_PIOC_OER = (1 << 14);    // make pin 49 as output
  *AT91C_PIOC_SODR = (1 << 14);  // pin 49 set c/d
  *AT91C_PIOC_OER = (1 << 15);
    
  *AT91C_PIOC_CODR = (1 << 15); //pin 48 clear chip select
  *AT91C_PIOC_OER = (1 << 16);
  *AT91C_PIOC_CODR = (1 << 16); // pin 47 clear read display

  delay(10);
  
  temp = (*AT91C_PIOC_PDSR)>>2; // Read data bus and save it in temp
  *AT91C_PIOC_SODR = (0x19 << 12); // pin 48 set chip select, pin 47 set read, pin 51 set because input is inverted
  *AT91C_PIOC_CODR = (1 << 13); // pin 50, Set dir as output (74chip)

  return temp;
}

void Write_Command_2_Display(unsigned char Command){
  //Wait until Read_Status_Display returns an OK
  while((Read_Status_Display() & (0x3)) != 0x3);
  *AT91C_PIOC_CODR = (0x3FC); // clear databus
  
  //Set Command to databus
  *AT91C_PIOC_SODR = ((unsigned int) Command << 2);
  
  //*AT91C_PIOC_CODR = (3 << 12); // pin 50 set dir as output and pin 51 enable output
  *AT91C_PIOC_CODR = (1 << 13); // pin 50 set dir as 0
  *AT91C_PIOC_CODR = (1 << 12); // 
  
  //Set databus as output
  *AT91C_PIOC_OER = (0x3FC);    // make all pins as output
  //Set C/D signal High (1 = Command)
  *AT91C_PIOC_SODR = (1 << 14); // set c/d
  *AT91C_PIOC_CODR = (5 << 15); // pin 48 clear chip select and clear write display
  
  delay(10); 
  *AT91C_PIOC_SODR = (0x29 << 12);  //Set chip enable display, set write display, disable output
  *AT91C_PIOC_ODR = (0x3FC);    // set all pins as input

}

void Write_Data_2_Display(unsigned char Data){
  //Wait until Read_Status_Display returns an OK
  while((Read_Status_Display() & (0x3)) != 0x3);
    
  //*AT91C_PIOD_SODR = (1 << 2); // turn off 74-chip for keypad
  
  //*AT91C_PIOC_OER = (0x3FC);    // make all pins as output
  
  *AT91C_PIOC_CODR = (0x3FC); // clear databus
  //Set Command to databus
  *AT91C_PIOC_SODR = ((unsigned int) Data << 2);
  *AT91C_PIOC_CODR = (1 << 13); // pin 50 set dir as output
  *AT91C_PIOC_CODR = (1 << 12); // enable output

  //Set databus as output
  *AT91C_PIOC_OER = (0x3FC);    // make all pins as output
  *AT91C_PIOC_CODR = (0xB << 14); // clear c/d, clear chip, clear write
  delay(10);
  *AT91C_PIOC_SODR = (0x29 << 12);  //Set chip enable/select display, set write display, disable output
  *AT91C_PIOC_ODR = (0x3FC);    // set all pins as input
}

void Init_Display() {
  //*AT91C_PIOC_OER = 1;
  *AT91C_PIOD_CODR = (1); //   Clear Reset display
  delay(100);
  *AT91C_PIOD_SODR = (1); //   Set Reset display
    
  Write_Data_2_Display(0x00);
  Write_Data_2_Display(0x00);
  Write_Command_2_Display(0x40);//Set text home address
  Write_Data_2_Display(0x00);
  Write_Data_2_Display(0x40);
  Write_Command_2_Display(0x42); //Set graphic home address
  Write_Data_2_Display(0x1e);
  Write_Data_2_Display(0x00);
  Write_Command_2_Display(0x41); // Set text area
  Write_Data_2_Display(0x1e);
  Write_Data_2_Display(0x00);
  Write_Command_2_Display(0x43); // Set graphic area
  Write_Command_2_Display(0x80); // text mode
  Write_Command_2_Display(0x94); // Text on graphic off
  
}

int find(char c) {
  char alphabet[] = {" !'#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[/]^_`abcdefghijklmnopqrstuvwxyz{|}~"};
  for(int i=0; i< (sizeof(alphabet)/sizeof(alphabet[0])); i++){
    if(alphabet[i] == c) return i;
  }
  return -1;
}

void Print_String_2_Display(char string[], int size){
  for(int i = 0; i < size; i++){
    Write_Data_2_Display(find(string[i]));
    Write_Command_2_Display(0xC0);
    pointer++;
  }
}

/**
 * Returns the position of the Pointer
*/
int pointerPosition(){
  return pointer;
}

/**
 * Automates the position of the row
*/
void setRow(){
  Write_Data_2_Display(row*30);
  Write_Data_2_Display((row*30)>>8);
  Write_Command_2_Display(0x24);
  row++;
  pointer = row*30;
}

// If we want to set a specific row
void setRowNr(int y){
  row = y;
  Write_Data_2_Display(row*30);
  Write_Data_2_Display((row*30)>>8);
  Write_Command_2_Display(0x24);
  row++;
  pointer = row*30;
}

/**
 * setColumn(int x) basically sets the pointer somewhere on the line,
 * The row only has 30 seats so keep that in mind and will be ignored
 * if x > 30.
*/
void setColumn(int x){
  column = row+(x%30);
  pointer = column;
  Write_Data_2_Display(column);
  Write_Data_2_Display(column >> 8);
  Write_Command_2_Display(0x24);
}

// resetPointer() changes the position of the cursor to 0
void resetPointer(){
  Write_Data_2_Display(0);
  Write_Data_2_Display(0);
  Write_Command_2_Display(0x24); // Set Pointer
  pointer = 0;
}

/**
 * Due to wanting a Automated row function we needed two functions
 * one for the reset the position and one to reset the Row.
*/
void resetCursor(){
  row = 0;
  resetPointer();
}

void displayClear(){                             
  resetPointer();
  for(int i = 0; i < 30*16; i++){                       
    Write_Data_2_Display(0);
    Write_Data_2_Display(0);
    Write_Command_2_Display(0xC0);
  }
  resetPointer();
}

void fillDisplay(){
  for(int i = pointerPosition(); i < 30*16; i++){
    Write_Data_2_Display(0);
    Write_Command_2_Display(0xC0);
  }
  Print_String_2_Display("-----------------------------", 30);
}