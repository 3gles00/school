#include "at91sam3x8.h"
#include "system_sam3x.h"
#include "stdio.h"
#include "globals.h"
#include "Display.h"
#include "servo.h"

void adc_measurement(){
  *AT91C_ADCC_CHER = (3 << 1); //Enables CH 1 - 2
  *AT91C_ADCC_CR = (1 << 1); // Start Conversion
  *AT91C_ADCC_IER  = (3<<1);
}

void adc_Init(){
  *AT91C_PMC_PCER1 = (1<<5);//adc 
  *AT91C_ADCC_MR = (2<<8); // Set prescaler to 2   
  adc_measurement();
}

void adc_Handler(){
  int one = (*AT91C_ADCC_CDR1) & (0xFFF);
  int two = (*AT91C_ADCC_CDR2) & (0xFFF);
  westLight = ((double)one) / 4095 * 3.3f;
  eastLight = ((double)two) / 4095 * 3.3f;
  *AT91C_ADCC_CR = 2; // Start ADC conversion
}

void adc_main(){
  adc_Handler();
  servo_move();
  delay(25);
}


