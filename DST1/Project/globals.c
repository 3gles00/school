#include "at91sam3x8.h"
#include "system_sam3x.h"
#include "stdio.h"
#include "temp.h"
#include "date.h"
#include "LinkedList.h"
#include "LED.h"
#include "lightSensor.h"
#include "keypad.h"
#include "lightShade.h"
#include "Display.h"

void delay(int a){
  for(int i = 0; i < a; i++);
}

unsigned int
  lightGainedSeconds = 0,
  SYSTEM_TICK = 0,
  timeTick = 0,
  REG_TIME = 0,
  REG_DATE = 0,
  cursorRow = 0,
  servoDegrees = 0,
  upperLimit = 25,
  lowerLimit = 20,
  dayCounter = 0; // Recording the initial day between 1 - 7 for temperature
double 
  temperature = 0,
  westLight = 0,
  eastLight = 0;
  //Flags
char
  fastModeFlag = 0,
  menuFlag = 0,
  printTimeFlag = 0,
  artificialLightStatus = 0,
  timeString[9],
  dateString[11];

int fastModeStatus(){
  if(fastModeFlag == 1)
    return 1;
  return 0;
}

void fastModeON(){
  fastModeFlag = 1;
}

void fastModeOFF(){
  fastModeFlag = 0;
}

void fastModeMenu(){
  displayClear();
  setRowNr(3);
  char c[17];
  int status = fastModeStatus();
  if(status == 1)
    sprintf(c, "Fast Mode is On ");
  else sprintf(c, "Fast Mode is OFF");
  Print_String_2_Display(c, 17);
  setRow();
  setRow();
  Print_String_2_Display("In Fast Mode each second is", 28);
  setRow();
  Print_String_2_Display("30 Min", 8);
  setRow();
  setRow();
  Print_String_2_Display("1: Fast Mode ON", 16);
  setRow();
  setRow();
  Print_String_2_Display("3: Fast Mode OFF", 16);
  setRow();
  setRow();
  Print_String_2_Display("*: Return", 10);
  fillDisplay();
  switch (validKeyPress()){
  case 1:
    fastModeON();
    fastModeMenu();
    break;
  case 2:
    fastModeOFF();
    break;
  case 3:
    fastModeOFF();
    fastModeMenu();
  case 4:
    fastModeOFF();
    break;
  case 5:
    fastModeOFF();
    break;
  case 6:
    fastModeOFF();
    break;
  case 7:
    fastModeOFF();
    break;
  case 8:
    fastModeOFF();
    break;
  case 9:
    fastModeOFF();
    break;
  case 10:
    fastModeOFF();
    break;
  case 11:
    fastModeOFF();
    break;
  case 12:
    fastModeOFF();
    break;
  }
}

void SysTick_Handler(){
  SYSTEM_TICK++;
  if(SYSTEM_TICK % 100 == 0){
    adc_main();
    if((upperLimit < temperature || lowerLimit > temperature))
      alarm();
  } 
  if(SYSTEM_TICK % 1000 == 0){
    artificalSun();
    temp_main();
    TC0_Handler();
  }
  if(menuFlag == 1){
    if(fastModeFlag == 1){
      if(SYSTEM_TICK % 1000/30 == 0){
        timeIncrement();
        insertData();
      }
    }
    else{
      if(SYSTEM_TICK % 1000 == 0){
        timeIncrement();
        if(SYSTEM_TICK % 60000 == 0){
          insertData(); 
        }
      }
    }
    if(SYSTEM_TICK % 1000 == 0) printCurrentTime();
  }
  if(SYSTEM_TICK % 60000 == 0) SYSTEM_TICK = 0;
}
