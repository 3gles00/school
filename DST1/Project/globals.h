#ifndef _GLOBALS_H
#define _GLOBALS_H


extern unsigned int 
  lightGainedSeconds,
  SYSTEM_TICK,
  timeTick,
  REG_TIME,
  REG_DATE,
  cursorRow,
  servoDegrees,
  upperLimit,
  lowerLimit,
  dayCounter;
extern double 
  temperature,
  westLight,
  eastLight,
  tempAvg;
extern char
  fastModeFlag,
  menuFlag,
  printTimeFlag,
  artificialLightStatus,
  timeString[9],
  dateString[11];

int fastModeStatus();
void delay(int value);
int fastModeStatus();
void fastModeMenu();
// void Systick_Handler(); // Should not be called

#endif
