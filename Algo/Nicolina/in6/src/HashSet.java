package src;

import java.util.PriorityQueue;
import java.util.Comparator;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

class HashSet {

    private static HashElement[] array;
    private static int size;
    private static int totalWords;

    /**
     * Settings up the HashSet for usage takes in int to decide size for
     * HashSetLinkedList if 
     * @param capacticy
     */
    public HashSet(int capacticy) {
        array = new HashElement[nextPrime(capacticy)];
        size = 0;
        totalWords = 0;
    }

    /**
     * Returns boolean depending of the number given is a prime number
     * @param number
     * @return
     */
    public static boolean isPrime(int number) {
        if (number % 2 == 0) {
            return false;
        }
        for (int i = 3; i * i <= number; i += 2) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * If the number given is not prime it corrects it
     * @param i
     * @return
     */
    public static int nextPrime(int i) {
        while (isPrime(i) == false) {
            i++;
        }
        return i;
    }

    /**
     * inserts a HashElement into the HashSet, if the HashSet already contains the element
     * it will increment that HashElement by 1. 
     * 
     * @param x
     */
    public void insert(String x) {
        int hash = hashCode(x, array.length);
        HashElement temp = array[hash];
        if(contains(x)){
            if(array[hash].getName() == x) array[hash].increment();
            else{
                while(temp.next != null){
                    if(temp.next.getName() == x){
                        temp.next.increment();
                        HashElement temp2 = temp.next.next;
                        temp.next.next = array[hash];
                        array[hash] = temp.next;
                        temp.next = temp2;
                        break;
                    }
                    temp = temp.next;
                }
            }
        } 
        else{
            array[hash] = new HashElement(x);
            array[hash].next = temp;
            size++;
        }
        totalWords++;
    }

    public boolean remove(String x) {
        int i = hashCode(x, array.length);
        if (array[i] != null) {
            if (array[i].getName() == x) {
                array[i].changeStatus();
                return true;
            }
            HashElement temp = array[i];
            while (temp != null) {
                temp = temp.next;
                if (temp.getName() == x) {
                    temp.changeStatus();
                    if (!temp.changeStatus()) {
                        totalWords -= temp.getCounter();
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public static int hashCode(String key, int tableSize){
        int hashVal = 0;
        for(int i = 0; i < key.length(); i++){
            hashVal += key.charAt(i);
        }
        return hashVal%tableSize;
    }

    public boolean contains(String x){
        HashElement temp = array[hashCode(x, array.length)];       
        boolean status = false;
        while(temp != null){
            if(temp.getName() == x){
                status = true;
            }
            temp = temp.next;
        }
        return status;
    }

    public String[] tableInOrder() {
        PriorityQueue <String> priority = new PriorityQueue <String> (size);
        for (int i = 0; i < array.length; i++) {
            HashElement temp = array[i];
            while (temp != null) {
                if (temp.getStatus()) {
                    priority.add(temp.getName());
                }
                temp = temp.next;
            }
        }
        String[] inOrder = new String[size];
        for(int i = 0; i < size; i++){
            inOrder[i] = priority.peek();
            priority.remove(inOrder[i]);
        }

        return inOrder;
    }

    public HashElement[] getMaxWordsFrequency() {
        HashElement[] unsortedArray = new HashElement[size];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            HashElement temp = array[i];
            while (temp != null) {
                if (temp.getStatus()) {
                    unsortedArray[j] = temp;
                    j++;
                }
                temp = temp.next;
            }
        }

        PriorityQueue < HashElement > pq = new PriorityQueue < HashElement > (size, new Comparator < HashElement > () {
            public int compare(HashElement lhs, HashElement rhs) {
                if (lhs.getCounter() < rhs.getCounter()) return +1;
                if (lhs.getCounter() == rhs.getCounter()) return 0;
                return -1;
            }
        });
        for (int i = 0; i < size; i++) {
            pq.add(unsortedArray[i]);
        }
        for(int i = 0; i < size; i++){
            unsortedArray[i] = pq.peek();
            pq.remove(unsortedArray[i]);
        }
        return unsortedArray;
    }

    public static void main(String[] args) throws IOException {
        HashSet table = new HashSet(30);
        try {
            BufferedReader scan = new BufferedReader(new FileReader("school/AlgorithmesAndComputerStructures/in6/LoremIpsum.txt"));
            String input;
            String[] split;
            while ((input = scan.readLine()) != null) {
                split = input.split("\\W+");
                for(int i = 0; i < split.length; i++){
                    table.insert(split[i]);
                }
            }
            String[] inOrder = table.tableInOrder();
            System.out.println("Table Printed in Alphabetical Order");
            for (int i = 0; i < inOrder.length; i++) {
                System.out.println(inOrder[i]);
            }
            HashElement[] frq = table.getMaxWordsFrequency();
            System.out.println("Printing table in most common words used");
            for(int i = 0; i < frq.length; i++){
                System.out.println(frq[i].getName() + " was used " + frq[i].getCounter() + "/" + totalWords);
            }
            scan.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}