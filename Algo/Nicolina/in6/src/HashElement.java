package src;

/**
* Class contians part of assignments from the course
* 'Algorithms and Computer Science' which is taught at
* Halmstad University
* @Author: Haron Obaid
*/
class HashElement {

    private String name; // ordet
    private int counter; // antalet förekomst av ordet
    private boolean isActive = false; // behövs eventuellt för remove
    public HashElement next;

    /**
     * Generates an Element to be used in HashSet
     * @param theWord
     */
    public HashElement(String theWord){
        name = theWord;
        isActive = true;
        counter = 1;
        next = null;
    }

    /**
     * Returns the name of the HashElement
     * @return
     */
    public String getName(){
        return name;
    }

    /**
     * Checksout @param o and changes it's active to false if it's active returns true if the name matches
     * 
     * @param o
     * @return
     */
    public boolean remove(HashElement o){
        if(name == o.name){
            changeStatus();
            return true;
        }
        return false;
    }

    /**
     * adds +1 to the Elements counter meaning that there is one more of it's kind
     */
    public void increment(){
        counter++;
    }

    /**
     * returns the int counter (how many element = name)
     * @return
     */
    public int getCounter(){
        return counter;
    }

    /**
     * Returns the status of the Elemnt.
     * @return 
     */
    public boolean getStatus(){
        return isActive;
    }

    /**
     * Used if you add an element or if you remove one from it,
     * changes the status of the Element
     * @return
     */
    public boolean changeStatus(){
        if(isActive){
            isActive = false;
        }
        else{
            isActive = true;
        }
        return isActive;
    } 
}