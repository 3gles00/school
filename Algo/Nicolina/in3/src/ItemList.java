package src;

/**
 * Klassen ItemList �r en l�nkad lista som inneh�ller noder av Item-objekt.
 * F�rsta noden i listan ( header) inneh�ller inget objekt.
 */

 /**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
public class ItemList {
   private Node header;
   int size;

   public ItemList() {
      header = new Node(null);
      size = 0;
   }

   /**
    * Skapar ett ny Node-objekt med ett nytt Item objekt och l�gger den i listan.
    * �kar v�rdet av variabel size;
    */
   public void add(Item item) {
      Node newNode = new Node(item);
      Node temp = header;
      while (temp.next != null) {
         temp = temp.next;
      }
      size++;
      temp.next = newNode;
   }

   /**
    * Skapar ett ny Node objekt med ett nytt Item-objekt och l�gger den i listan p�
    * plats index, �kar v�rdet av variabel size;
    */
   public void addAt(Item newItem, int index) {
      Node newNode = new Node(newItem);
      Node temp = header;
      if (size >= index && index >= 0) {
         for (int i = 0; i < index; i++) {
            if (temp.next != null) {
               temp = temp.next;
            }
         }
         size++;
         newNode.next = temp.next;
         temp.next = newNode;
      } else {
         System.out.println("Index out of bounds");
         System.out.println("Try another one");
      }
   }

   /**
    * Returnerar true om det finns ett Item-objekt vars RFIDNR �r lika med metodens
    * parametern id
    */
   public boolean find(String id) {
      Node node = header.next;
      while (node != null) {
         if (node.item.RFIDNR.equals(id)) {
            return true;
         }
         node = node.next;
      }
      return false;
   }

   /**
    * Ta bort noden som inneh�ller Item- objektet med viss id
    */
   public Item remove(String id) {
      Node node = header;
      Node temp = null;
      while (node.next != null) {
         if (node.next.item.getItemNumber().equals(id)) {
            size--;
            temp = node.next;
            node.next = node.next.next;
            break;
         }
         node = node.next;
      }
      return temp.item;
   }

   /**
    * R�knar och returnerar antalet Item- objekt med ett vis namn oavsett RFIDNR
    */

   public int countObjects(String name) {
      int count = 0;
      Node node = header.next;
      while (node != null) {
         if (node.item.name.equals(name)) {
            count++;
         }
         node = node.next;
      }
      return count;
   }

   /**
    * Skriver ut inneh�llet i listan
    */
   public void printList() {
      Node node = header.next;
      while (node != null) {
         System.out.println(node.item);
         node = node.next;
      }
   }

   /**
    * Returnera true om listan �r tom annars false
    */
   public boolean isEmpty() {
      if (size == 0) {

         return true;
      }
      return false;
   }

   /**
    * Returnera antlet element i listan
    */
   public int size() {
      return size;
   }

   public String getItemName(String name) {
      Node node = header.next;
      while (node != null) {
         if (node.item.getItemName().equals(name)) {
            return node.item.getItemName() + " " + node.item.getItemNumber();
         }
         node = node.next;
      }
      return null;
   }

   public String getItemNumber(String number) {
      Node node = header.next;
      while (node != null) {
         if (node.item.RFIDNR.equals(number)) {
            return node.item.name + " " + node.item.RFIDNR;
         }
         node = node.next;
      }
      return null;
   }

   public String getItem() {
      String nameAndNumb = "";
      Node node = header.next;
      while (node != null) {
         nameAndNumb += node.item.name + " " + node.item.RFIDNR + " ";
         node = node.next;
      }
      return nameAndNumb;
   }

   public Item itemComp(int index) {
      Node temp = header;
      for (int i = 0; i < index; i++) {
         temp = temp.next;
      }
      return temp.item;
   }
}