package src;

import java.util.*;
import java.io.*;

/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
public class LagerProgram {
   static Scanner scan = new Scanner(System.in);
   static ItemList inStore = new ItemList();
   static ItemList outStore = new ItemList();
   static long nr = 1111111111;
   static Date date = new Date();
   static PrintWriter newIn;
   static PrintWriter newOut;

   public static void main(String[] args) {
      try {
         Scanner filescan = new Scanner(new File("assets/Verktyg.txt"));
         while (filescan.hasNext()) {
            System.out.println(filescan.next());
         }
      }

      catch (IOException ex) {
         System.out.println("404 File not found");
      }
      printMenu();
      int choice = scan.nextInt();
      scan.nextLine();

      while (choice != 0) {
         dispatch(choice);
         printMenu();
         choice = scan.nextInt();
         scan.nextLine();
      }
   }

   public static String getRFID() {
      return "" + nr++;

   }

   public static void dispatch(int choice) {
      switch (choice) {
      case 0:
         System.out.println("EXIT");
         System.exit(0);
         break;

      case 1: {
         try {
            Scanner filescan = new Scanner(new File("assets/Verktyg.txt"));
            while (filescan.hasNext()) {
               inStore.add(new Item(filescan.next(), filescan.next()));
            }
         } catch (IOException ex) {
            System.out.println("404 File not found");
         }
         break;
      }
      case 2: {
         System.out.println("Enter your RFID number to your product");
         Scanner scanningRFID = new Scanner(System.in);
         String RFID = scanningRFID.nextLine();
         if (inStore.find(RFID)) {
            Item temp = inStore.remove(RFID);
            temp.setDeliverDate(date);
            System.out.println(temp);
            outStore.add(temp);
         } else {
            System.out.println("Produt is not in the Store at the moment check another time");
         }
         scanningRFID.close();
         break;
      }
      case 3: {
         System.out.println("Enter your RFID number to your product");
         Scanner scanningRFID = new Scanner(System.in);
         String RFID = scanningRFID.nextLine();
         if (outStore.find(RFID)) {
            Item temp = outStore.remove(RFID);
            temp.setDeliverDate(date);
            System.out.println(temp);
            outStore.add(temp);
         } else {
            System.out.println("Product is not in the Store check later.");
         }
         try {
            newIn = new PrintWriter("assets/Verktyg.txt");
            newOut = new PrintWriter("VerktygOut.txt");
            newOut.print(outStore.getItem());
            newOut.close();
            newIn.print(inStore.getItem());
            newIn.close();
         } catch (FileNotFoundException e) {
            e.printStackTrace();
         }
         scanningRFID.close();
         break;
      }

      case 4: {
         System.out.println("search for RFID or name:");
         Scanner value = new Scanner(System.in);

         if (value.hasNextInt()) {
            String temp = value.next();
            if (inStore.find(temp)) {
               System.out.println(inStore.getItemNumber(temp));

            } else {
               System.out.print("sorry not find anything. Int");
               break;
            }

         } else {
            String temp = value.nextLine();
            String holder = inStore.getItemName(temp);

            if (holder == null) {

               System.out.print("sorry not find anything. String");
               break;
            }
            System.out.println(holder);

         }
         value.close();
         break;
      }
      case 5: {
         outStore.printList();
         break;
      }

      case 6: {
         inStore.printList();
         break;
      }

      default:
         System.out.println("Sorry, invalid choice");
      }
   }

   // ----------------------------
   // Print User Menu
   // ----------------------------
   public static void printMenu() {
      System.out.println("\n  Welcome");
      System.out.println("   ====");
      System.out.println("0: Exit");
      System.out.println("1: Setup Store");
      System.out.println("2: Loan");
      System.out.println("3: Return");
      System.out.println("4: Find product");
      System.out.println("5: Out of Stock");
      System.out.println("6: In Stock");

      System.out.print("\nEnter your choice: ");
   }

}