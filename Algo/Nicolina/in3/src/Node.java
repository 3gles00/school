package src;

/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
public class Node {
    public Item item;
    public Node next;

    // Constructors

    public Node(Item item) {
        this(item, null);
    }

    public Node(Item item2, Node n) {
        item = item2;
        next = n;
    }

}