package src;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
public class SearchMethods {

    /**
     * Searches array with linear search
     * 
     * @param a
     * @param x
     * @return
     */
    public static int linearSearch(int[] a, int x) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] == x) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Searches with the algorithm binary search
     * 
     * @param a
     * @param x
     * @return
     */
    public static int binarySearch(int[] a, int x) {
        int begin = 0;
        int end = a.length - 1;
        while (begin <= end) {
            int m = ((begin + end) / 2);
            if (a[m] > x) {
                begin = m - 1;
            } else if (a[m] > x) {
                end = m - 1;
            } else
                return m;
        }
        return -1;
    }

    /**
     * Creates array at certain size between a parameter of numbers
     * 
     * @param size
     * @param min
     * @param max
     * @return
     */
    static int[] createArray(int size, int min, int max) {
        int[] ourArray = new int[size];
        for (int i = 0; i < ourArray.length; i++) {
            ourArray[i] = (int) Math.random() * (max - min) + min;
        }
        return ourArray;
    }

    /**
     * Calls for the program
     * 
     * @param args
     */
    public static void main(String[] args) {
        int[] goFor = createArray(100000, 1, 1000);
        try {
            PrintWriter textLin = new PrintWriter("LinearText.txt");
            for (int i = 0; i < 10; i++) { // 10 time
                long time1 = System.currentTimeMillis();// start time
                linearSearch(goFor, 45); // search will not take the same time becasue its take diffrent time to compare
                                         // values
                long time2 = System.currentTimeMillis(); // end time
                textLin.println("Time: " + (time2 - time1) + " Length: " + goFor.length); // write
            }
            textLin.close(); // close
            Arrays.sort(goFor); // sort array

            PrintWriter textBin = new PrintWriter("BinText.txt");

            for (int i = 0; i < 10; i++) { // 10 times
                long time1 = System.currentTimeMillis();// time start
                binarySearch(goFor, 45); // search will not take the same time becasue its take diffrent time to compare
                                         // values
                long time2 = System.currentTimeMillis();// time end
                textBin.println("Time: " + (time2 - time1) + " Length: " + goFor.length); // write
            }
            textBin.close(); // close
        } catch (FileNotFoundException e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }
}