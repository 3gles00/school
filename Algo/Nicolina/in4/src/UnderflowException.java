package src;

/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */

/**
 * Exception class for access in empty containers such as stacks, queues and
 * priority queues.
 * 
 * @author Mark Allen Weiss
 */
public class UnderflowException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructs this exception object.
     * 
     * @param message the error message.
     */
    public UnderflowException(String message) {
        super(message);
    }
}
