package src;

import java.util.LinkedList;

/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
public class RandomQueue {
    private LinkedList<Object> list;
    private int size;

    @SuppressWarnings("unused")
    private Object top, last;

    public RandomQueue() {
        list = new LinkedList<Object>();
        size = 0;
    }

    public Boolean isEmpty() {
        return size == 0;
    }

    public void enqueued(Object o) {
        if (o != null) {
            list.addFirst(o);
            last = list.getFirst();
            size++;
        }
    }

    public Object dequeued() {
        int rand = (int) (Math.random() * size);
        Object temp = list.remove(rand);
        size--;
        if (size != 0) {
            top = list.getLast();
        } else {
            System.out.println("Queue us empty");
        }
        return temp;
    }

    public static void main(String[] args) {
        RandomQueue queue = new RandomQueue();
        queue.enqueued("Mattias Alm");
        queue.enqueued("Andreas Andersson");
        queue.enqueued("Amanda Andersson");
        System.out.println(queue.isEmpty());
        System.out.println(queue.dequeued());
        System.out.println(queue.dequeued());
        System.out.println(queue.dequeued());
        System.out.println(queue.isEmpty());

    }
}
