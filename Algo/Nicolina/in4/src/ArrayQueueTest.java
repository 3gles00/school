package src;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
public class ArrayQueueTest {

    ArrayQueue<String> in;
    ArrayQueue<String> out;

    @Before
    public void setUp() {
        in = new ArrayQueue<String>();
        out = new ArrayQueue<String>();
    }

    @Test
    public void printQueueTest() {
        in.enqueue("A");
        in.enqueue("B");
        in.enqueue("C");
        in.enqueue("D");

        assertEquals("printqueue supposed to print the letters", "ABCD", in.printQueue());
    }

    @Test
    public void reverseTest() {
        in.enqueue("A");
        in.enqueue("B");
        in.enqueue("C");
        in.enqueue("D");
        out = in.reverse();

        assertEquals("reversed the String of in", "DCBA", out.printQueue());

    }
}
