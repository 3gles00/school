package src;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
public class ArrayStackTest<AnyType> {

    ArrayStack<String> stack1;
    ArrayStack<String> stack2;

    @Before
    public void setUp() {
        // Two new lists for differnt test cases
        stack1 = new ArrayStack<String>();
        stack2 = new ArrayStack<String>();
    }

    @Test
    public void copyTest() {
        stack1.push("A");
        stack1.push("B");
        stack1.push("C");
        stack1.push("D");

        stack2 = stack1.copy();

        assertEquals("copyStack removes D", "D", stack2.pop());
        assertEquals("copyStack removes C", "C", stack2.pop());
        assertEquals("copyStack removes B", "B", stack2.pop());
        assertEquals("copyStack removes A", "A", stack2.pop());
    }
}