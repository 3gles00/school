package src;

/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
class Print{
    
    /**
     * Function is recursive method that prints all the numbers from 0 - n.
     * Function will run n times.
     * @param n
     */
    public static void print(int n){
        if(n >= 0){
            System.out.println(n);
            print(n-1);
        }
    }

    /**
     * Testing the function above.
     * @param args
     */
    public static void main(String[] args){
        print(5);
    }
}