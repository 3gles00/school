package src;

/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
public class Palindrome {
    
    private static  int i = 0;

    /**
     * Function is a recursive method that checks if the argument is a valid palindrome.
     * @param word */
    public static void palindrome(String word){
        if(i == word.length()/2){
            System.out.println(word + " is a true palindrome");
        }
        else if(word.charAt(i) == word.charAt(word.length() - 1 - i)){
            i++;
            palindrome(word);
        }
        else{
            System.out.println(word + " is a false palindrome");
        }
     }

     public static void main(String[] args){
         palindrome("kajak");
         palindrome("grabbar");
     }
}
