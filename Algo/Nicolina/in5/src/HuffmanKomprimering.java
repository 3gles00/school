package src;

import java.io.*;
import java.util.*;

 /**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
class HuffmanKomprimering {

	private static ArrayList < HuffmanTree > theForest;
	private static int[] freqs = new int[256];
	private static String[] codes = new String[256];
	private static String nameFromString; 
	private static String fileName;

	private static void readFile(String filename) {	
		try{
			BufferedReader file = new BufferedReader(new FileReader(filename));
			int ascii = file.read();
			while(ascii != -1){
				if(!(ascii > 256)){
					if(codes[ascii] != null){
						if(codes[ascii].equals(Integer.toString(ascii))){ // Checks if character has been seen before
							freqs[ascii]++;
						}
					}		
					else{	
						codes[ascii] = Integer.toString(ascii); // Makes space for the character otherwise.
						freqs[ascii] = 1;
					}
				}
				ascii = file.read();
			}
			file.close();
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	private static void makeTree() {
		//skapar Huffman tr�d med ascii-kod i noden och frekvensen som vikt
		//l�gger till tr�det in theForest
		HuffmanTree tree1 = null;
		HuffmanTree tree2 = null;
		for(int i = 0; i < codes.length; i++){
			if(codes[i] != null){
				Integer temp = Integer.valueOf(codes[i]);
				tree1 = new HuffmanTree(temp, freqs[i]);
				theForest.add(tree1);
			}
		}
		do{
			theForest.sort(null);
			tree2 = (HuffmanTree) theForest.remove(1);
			tree1 = (HuffmanTree) theForest.remove(0);
			theForest.add(new HuffmanTree(tree1, tree2));
		}
		while(theForest.size() != 1);
		
	}


	private static void makeCods() { 
		HuffmanTree Node = (HuffmanTree) theForest.get(0);
		Node.codes(codes); 

	}



	private static void showResults() {
		System.out.println("The following characters was used: ");
		System.out.println("----------------------------------");
		for (int i = 0; i < codes.length; i++){
			if(codes[i] != null){
				char value = (char) i;
				System.out.println(value + " " + freqs[i] + " " + codes[i]);
			}
		}
	}

	@SuppressWarnings("resource")
	private static void makeFile() {
		try{
			FileOutputStream compressFile = new FileOutputStream("school/AlgorithmesAndComputerStructures/in5/" + nameFromString + "Encrypted.txt");
			DataOutputStream compressor = new DataOutputStream(compressFile);
			BufferedReader file = new BufferedReader(new FileReader(fileName));
			int ascii = file.read();
			String placeholder = "";
			while(ascii != -1){
				if(!(ascii > 256)){
					placeholder += codes[ascii];
				}
				ascii = file.read();
			}
			String temp = "";
			int count = 0;
			for(int i = 0; i < placeholder.length(); i++){
				temp += placeholder.charAt(i);
				if(temp.length() == 8){ // can only hold 8 bytes at a time
					count++;
					compressor.write(stringTobyte(temp));
					temp = "";
				}
			}
			compressFile.write(Integer.parseInt(temp));
			System.out.println(count);
			compressor.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}


	// metoden tar som argument en str�ng (11000111) och konvertera den till byte
	// anv�nd metoden f�r att konvertera dina koder till byte som skall sedan sparas till filen. 

	public static byte stringTobyte(String s) {
		byte b = 0;
		if (s.length() != 8) {
			throw new RuntimeException("The string representation of the byte in not of proper length!");
		}
		for (int i = 0; i < 8; i++) {
			if (s.charAt(i) == '1'){	
				b |= (1 << (7 - i));
			}

		}
		return b;
	}

	public static void main(String[] a) {
		theForest = new ArrayList <HuffmanTree> ();
		Scanner scan = new Scanner(System.in);
		System.out.println("Give us the name of the file");
		nameFromString = scan.nextLine();
		fileName = "school/AlgorithmesAndComputerStructures/in5/" + nameFromString + ".txt";
		readFile(fileName);
		makeTree();
		HuffmanTree temp = (HuffmanTree) theForest.get(0);
		System.out.println(temp.getWeight());
		makeCods();
		showResults();
		makeFile();
		scan.close();
	}
}