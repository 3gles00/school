package src;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;


/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
public class TreeSetScan {
    
    public static void main(String[] args){
        // If this path doesn't work for you you should try fixed path from C:
        File file = new File("school\\AlgorithmesAndComputerStructures\\in5\\klasslista.txt");
        Scanner scan;
        try{
            scan = new Scanner(file);
            TreeSet<String> reader = new TreeSet<String>();
            while(scan.hasNextLine()){
                reader.add(scan.next());
            }

            Iterator<String> iterating = reader.iterator();
            while(iterating.hasNext()){
                System.out.println(iterating.next());
            }
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
    }
}
