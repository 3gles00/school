package src;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;

/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
public class TreeSetCounter{

    private static TreeSet<Itm> tree = new TreeSet<Itm>();

    /**
     * Adds Items to the tree
     * @param word
     */
    public static void addWord(String word){
        Iterator<Itm> itr = tree.iterator();
        boolean search = false;
        while(itr.hasNext() && !search){
            Itm temp = itr.next();
            if(temp.getName().equals(word)) {
                search = true;
                tree.add(new Itm(word, 1));
            }
        }
        if(!search){
            tree.add(new Itm(word, 1));
        }
    }

    /**
     * Clears the tree
     */
    public static void makeEmpty(){
        tree.clear();
    }


    /**
     * Cecks the tree for the most used item 
     */
    @SuppressWarnings("unused")
    public static void getMaxFreq(){
        Iterator<Itm> itr = tree.iterator();
        Itm max = new Itm("", 0);
        while(itr.hasNext()){
            Itm temp = itr.next();
            int count;
            String name;
            if(max.getCounter() < temp.getCounter()){
                count = max.getCounter();
                name = max.getName();
                max.setName(temp.getName());
            }
        }
        System.out.println("Most Frequent is: " + max.getName());
        System.out.println("Character was printed " + max.getCounter() + " times");
    }

    /**
     * Prints Tree
     */
    public static void printTree(){
        Iterator<Itm> itr = tree.iterator();
        int count = 0;
        System.out.println("Full List");
        
        while(itr.hasNext()){
            Itm temp = itr.next();
            count++;
            System.out.println("-----------------------------------------------");
            System.out.println(count + ". " + temp.getName() + " was used "+ temp.getCounter() + " times");
        }
        if(count == 0){
            System.out.println("Tree is Empty");
        }
    }

    /**
     * Generates the Program, adds items to the tree for the functions to use
     * @param args
     */
    public static void main(String[] args){
        Scanner scan;
        try{
            scan = new Scanner(new File("school/AlgorithmesAndComputerStructures/in5/klasslista.txt"));
            while(scan.hasNext()){
                addWord(scan.nextLine());
            }
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        getMaxFreq();
        printTree();
        makeEmpty();
        printTree();
    }
}
