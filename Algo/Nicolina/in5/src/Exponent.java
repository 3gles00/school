package src;

/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
public class Exponent{
    
    /**
     * This a recursive function that which basically works as Math.pow(base, exponent).
     * Function will run exponent amount of times.
     * @param base
     * @param exponent
     * @return
     */
    public static int exponite(int base, int exponent){
        if(exponent <= 0){
            return 1;
        }
        else{
            return exponite(base, exponent - 1)*base;   
        }
    } 


    /**
     * Testing the function above with the main method.
     * @param args
     */
    public static void main(String[] args){
        System.out.println(exponite(3, 4));
    }
}