package src;

public class Itm implements Comparable<Itm> {
    
    private String name;
    private int counter;

    public Itm(String itemName, int itemCount){
        name = itemName;
        counter = itemCount;
    }

    public String getName(){
        return name;   
    }

    public void setName(String newName){
        this.name = newName;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int newInt){
        this.counter = newInt;
    }

    @Override
    public int compareTo(Itm newItem){
        return name.compareTo(newItem.getName());
    }
}
