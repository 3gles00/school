package src;

import java.io.*;
import java.util.*;

/**
 * Class contains the 5th part of the assignments 'Inlamningsuppgift 1' from the
 * course 'Algorithmes and Computer' Structures which is taught at 'Hogskolan i
 * Halmstad'.
 * 
 * @Author: Haron Obaid
 */
public class CharacterFrequency {

    /**
     * Function takes in a file and calculates the frequency of the different
     * letters.
     * 
     * @param path
     * @return
     */
    public static int[] Frequency(String path) {
        int[] frq = new int[127];
        File file = new File(path);
        int counter = 0;
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                char[] line = sc.nextLine().toCharArray();
                for (char i = 0; i < 127; i++) {
                    char character = i;
                    for (int j = 0; j < line.length; j++) {
                        if (line[j] == character) {
                            frq[i]++;
                            counter++;
                        }
                    }
                }
            }
            sc.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Letter   Amount    Frequency");
        for (char i = 65; i < 127; i++) {
            char letter = i;
            if (i == 91)
                i = 97;
            float temp = (float)frq[i];
            float temp2 = (float)counter;
            float value = temp/temp2;
            System.out.println(letter + "           " + frq[i] + "      " + value + "%");
        }
        return frq;
    }

    /**
     * Generates Program
     * 
     * @param args
     */

    public static void main(String[] args) {
        Frequency("school/AlgorithmesAndComputerStructures/in1/klasslista.txt"); // Adjust path according to own System Requirements
    }
}