package src;

import java.util.*;

/**
 * Class contians the third part of the assignments 'Inlamningsuppgifter 1'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class AddAtIndex {

    /**
     * Function takes in an array, an index and an int. It places the int at the
     * index of the array and returns it.
     * 
     * @param a
     * @param x
     * @param index
     * @return
     */
    public static String add(int[] a, int x, int index) {
        int[] b = new int[a.length * 2];
        int j = 0;
        for (int i = 0; i < a.length; i++) {
            b[j] = a[i];
            if (x == index) {
                j++;
                b[j] = x;
            }
            j++;
        }
        return Arrays.toString(b);
    }

    /**
     * Generates Program.
     * 
     * @param args
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random rand = new Random();
        System.out.println("Give us amount of numbers, a number to input and a index for said number");
        int length = scan.nextInt();
        int[] array = new int[length];
        int number = scan.nextInt();
        int index = scan.nextInt();
        scan.close();
        for (int i = 0; i < array.length; i++) {
            array[i] = rand.nextInt(99) + 1;
        }
        System.out.println(Arrays.toString(array));
        System.out.println(add(array, number, index));
    }
}
