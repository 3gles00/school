package src;

/**
 * Class contains the first 2 parts of the assignments 'Inlamningsuppgift 1'
 * from the course 'Algorithmes and Computer' Structures which is taught at
 * 'Hogskolan i Halmstad'.
 * @Author: Haron Obaid
 */
import java.util.*;

public class StringAndArrays {

    public static Random rand = new Random();

    /**
     * Function generates a password between 8-12 characters. The password will have
     * numbers, small and big letters.
     */
    public static String generatePassword() {
        int length = rand.nextInt(5) + 8;
        String numbers = "1234567890";
        String smallLetters = "abcdefghijklmnopqrstuvwxyz";
        String bigLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder password = new StringBuilder();

        for (int i = 0; i < length; i++) {
            int generator = rand.nextInt(3);
            char[] placeholder;
            int picker;
            if (generator == 0) {
                placeholder = numbers.toCharArray();
                picker = rand.nextInt(numbers.length());
            } else if (generator == 1) {
                placeholder = smallLetters.toCharArray();
                picker = rand.nextInt(smallLetters.length());
            } else {
                placeholder = bigLetters.toCharArray();
                picker = rand.nextInt(bigLetters.length());
            }
            password.append(placeholder[picker]);
        }
        return password.toString();
    }

    /**
     * Function checks a string for the valid notations of a password: number, small
     * and big character letters.
     * 
     * @param pass
     * @return
     */
    public static boolean passwordCheck(String pass) {
        char[] password = pass.toCharArray();
        boolean state = false, big = false, small = false, numb = false;

        char letter = 0;
        for (int i = 0; i < password.length; i++) {
            for (int j = 0; j < 123; j++) {
                letter++;
                if (j >= 48 && j <= 57 && password[i] == letter) {
                    numb = true;
                }
                if (j >= 65 && j <= 90 && password[i] == letter) {
                    big = true;
                }
                if (j >= 97 && j <= 122 && password[i] == letter) {
                    small = true;
                }
                if (big == true) {
                    if (small == true) {
                        if (numb == true) {
                            state = true;
                        }
                    }
                }
            }
        }
        return state;
    }

    /**
     * Generates Program.
     * 
     * @param args
     */
    public static void main(String[] args) {
        int boolCount = 0;
        for (int i = 0; i < 1000; i++) {
            System.out.println(generatePassword());
            System.out.println(passwordCheck(generatePassword()));
            if (passwordCheck(generatePassword())) {
                boolCount++;
            }
        }
        float total = boolCount / 1000;
        System.out.println(boolCount + " / " + 1000 + " = " + total);
    }
}