package src;

import java.util.Arrays;
import java.util.Random;

/**
 * Class contains the 5th part of the assignments 'Inlamningsuppgift 1' from the
 * course 'Algorithmes and Computer' Structures which is taught at 'Hogskolan i
 * Halmstad'.
 * 
 * @Author: Haron Obaid
 */
public class MergeArrays {

    /**
     * Function merges two arrays together and sorts them. If the array item = null
     * it will return a 0 in it.
     * 
     * @param a
     * @param b
     * @return
     */
    public static int[] mergeArrays(int[] a, int[] b) {
        int[] c = new int[a.length + b.length];
        for (int i = 0; i < a.length; i++) {
            c[i] = a[i];
        }
        for (int i = 0; i < b.length; i++) {
            c[a.length + i] = b[i];
        }
        Arrays.sort(c);
        return c;
    }

    /**
     * Takes an array and checks if it's sorted. Returnes boolean true or false if
     * the array is sorted.
     * 
     * @param a
     * @return
     */
    public static boolean isSorted(int[] a) {
        boolean sorted = false;
        int[] temp = a;
        Arrays.sort(temp);
        if (a == temp) {
            sorted = true;
        }
        return sorted;
    }

    /**
     * Generates the program.
     * 
     * @param args
     */
    public static void main(String[] args) {
        Random rand = new Random();
        int count = 0;
        while (count != 20) {
            int[] a = new int[rand.nextInt(25) + 1];
            int[] b = new int[rand.nextInt(25) + 1];
            for (int i = 0; i < a.length; i++) {
                a[i] = rand.nextInt(100) + 1;
            }
            for (int j = 0; j < b.length; j++) {
                b[j] = rand.nextInt(100) + 1;
            }
            System.out.println(Arrays.toString(mergeArrays(a, b)));
            System.out.println(isSorted(mergeArrays(a, b)));
            count++;
        }
    }
}