package src;

/**
 * Class contains the 7th part of the assignments 'Inlamningsuppgift 1'
 * from the course 'Algorithmes and Computer' Structures which is taught at
 * 'Hogskolan i Halmstad'.
 * @Author: Haron Obaid
 */
import java.util.*;
import java.io.*;

public class MakeArrayList {

    private static ArrayList<String> nameList = new ArrayList<>();

    /**
     * Function takes in ArrayList and index to catch a name and return it.
     * 
     * @param list
     * @param index
     * @return
     */
    public static String findName(ArrayList<String> list, int index) {
        String name = "";
        if (list.size() > index) {
            name = list.get(index).toString();

        } else {
            name = "Index was bigger than lines on the list";
        }
        return name;
    }

    /**
     * Function takes ArrayList and a name to delete from list. If the name was not
     * found it prints: 'name' was not found and returns false.
     * 
     * @param list
     * @param name
     * @return
     */
    public static boolean deleteName(ArrayList<String> list, String name) {
        boolean nameState = false;
        if (list.contains(name)) {
            list.remove(name);
            nameState = true;
        } else {
            System.out.println(name + " was not found");
        }
        return nameState;

    }

    /**
     * Function takes ArrayList and a string to put in list. It then sorts the list
     * 
     * @param list
     * @param name
     * @return
     */
    public static boolean addToList(ArrayList<String> list, String name) {
        boolean nameState = false;
        list.add(name);
        list.sort(null);
        if (list.contains(name)) {
            nameState = true;
        }
        return nameState;

    }

    /**
     * Generates Program with switch
     * 
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        File file = new File("school/AlgorithmesAndComputerStructures/in1/klasslista.txt"); // Change
                                                                                                           // path
                                                                                                           // depending
                                                                                                           // on System
                                                                                                           // and editor
                                                                                                           // used.
        Scanner data = new Scanner(file);
        while (data.hasNextLine()) {
            nameList.add(data.nextLine() + "\n");
        }
        data.close();
        Scanner scan = new Scanner(System.in);
        boolean again = true;
        while (again) {
            System.out.println("What do we want to do? checkout/delete/add a name?");
            String toDo = scan.nextLine();
            switch (toDo) {
            case "checkout":
                System.out.println("what name should we checkout? Give us an int.");
                int index = scan.nextInt();
                System.out.println(findName(nameList, index));
                break;

            case "delete":
                System.out.println("What name should we remove?");
                String nameToDelete = scan.nextLine();
                System.out.println(deleteName(nameList, nameToDelete));
                if (deleteName(nameList, nameToDelete)) {

                    System.out.println(nameToDelete + " was removed from the list");
                } else {
                    System.out.println(nameToDelete + " was not removed from the list");
                }
                break;

            case "add":
                System.out.println("Name to add?");
                String nameToAdd = scan.nextLine();
                System.out.println(addToList(nameList, nameToAdd));
                if (addToList(nameList, nameToAdd)) {
                    System.out.println("Name was added");
                } else {
                    System.out.println("Name was not added");
                }
                break;
            }
            System.out.println("Do you want to go again?");
            String cont = scan.nextLine();
            if (cont == "No") {
                again = false;
            }
        }
        scan.close();
        System.out.println(Arrays.toString(nameList.toArray()));
    }
}
