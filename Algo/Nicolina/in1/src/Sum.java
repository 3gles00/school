package src;

/**
 * Class contains the fourth part of the assignments 'Inlamningsuppgift 1'
 * from the course 'Algorithmes and Computer' Structures which is taught at
 * 'Hogskolan i Halmstad'.
 * @Author: Haron Obaid
 */
import java.util.*;

public class Sum {

    /**
     * Takes in an array and sums all of the values.
     * 
     * @param a
     * @return
     */
    public static int sumOfArray(int[] a) {
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
        }
        return sum;
    }

    /**
     * Generates Program with all of the arrays.
     * 
     * @param args
     */
    public static void main(String[] args) {
        Random rand = new Random();
        int x = rand.nextInt(19) + 1;
        int[][] twoD = new int[x][x];
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < x; j++) {
                int generate = rand.nextInt(8) + 1;
                twoD[i][j] = generate;
            }
        }
        System.out.println(Arrays.toString(twoD));
        for (int i = 0; i < x; i++) {
            int[] row = new int[x];
            int[] column = new int[x];
            for (int j = 0; j < x; j++) {
                row[j] = twoD[i][j];
                column[j] = twoD[j][i];
            }
            System.out.println("Sum of row " + i + ": " + sumOfArray(row));
            System.out.println("Sum of column " + i + ": " + sumOfArray(column));
        }
    }
}