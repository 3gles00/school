package src;

import java.util.Comparator;

/**
 * <h1>CountrymainProgram.java</h1>
 * <p>
 * This is a comparator that can help collections.sort() with the sort in the
 * sort i want it to be if <br/>
 * it will be in the country name or area or population.
 * </p>
 * 
 * @author Haron Obaid
 * @since 2021-03-22
 * @version 1.0
 */

public class CompareCountry implements Comparator<Object> {
    public int compare(Object land, Object oLand) {

        // parameter are of type Object, so we have to downcast it to country objects

        String landName = ((Land) land).getCountryName();
        String oLandName = ((Land) oLand).getCountryName();

        // uses compareTo method of String class to compare names of the employee
        return landName.compareTo(oLandName);

    }

}
