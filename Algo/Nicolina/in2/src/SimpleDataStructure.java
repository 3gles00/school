package src;

/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
public class SimpleDataStructure {
    private String[] friends;
    private int counter;

    /**
     * Generates Data Structure
     */
    public SimpleDataStructure() {
        friends = new String[5];
        counter = 0;
    }

    /** Appends the other friend name to the end of this
     *  list. If the list has to little spots it doubles.
     */
    public boolean add(String other) {
        if (counter >= friends.length) {
            String[] temp = friends;
            friends = new String[counter * 2];
            for (int i = 0; i < counter; i++) {
                friends[i] = temp[i];
            }
        }
        friends[counter] = other;
        counter++;
        return true;
    }

    /** returns the name at the specified index*/
    public String get(int index) {
        String nullify = null;
        if (index > friends.length) {
            return nullify;
        }
        return friends[index];
    }

    /** removes the first occurrence of the specified element
    in this list if the list contains the name*/
    public boolean remove(String name) {
        for (int i = 0; i < counter; i++) {
            if (friends[i].equals(name)) {
                friends[i] = null;
                return true;
            }
        }
        return false;
    }

    /**
     * Sorts List
     */
    public void addSort(String name) {
        add(name);
        System.out.println("pointer");
        for (int i = 0; i < counter; i++) { // Bubblesort method
            for (int j = 1; j < counter; j++) {
                if (friends[j].compareTo(friends[j - 1]) < 0) {
                    String temp = friends[j]; // Hold the value to switch positions
                    friends[j] = friends[j - 1];
                    friends[j - 1] = temp;
                }
            }

        }
    }

    /** prints all names in the array friends
     *  skips place if null
     */
    public void printFriends() {
        System.out.println("Printing Array");
        for (int i = 0; i < friends.length; i++) {
            if (friends[i] != null) {
                System.out.print(friends[i] + " ");
                System.out.println();
            }
        }
    }

    public static void main(String[] arg) {
        SimpleDataStructure myfriends = new SimpleDataStructure();
        String[] names = {
            "Saga",
            "Darius",
            "Anders",
            "Mustafa",
            "Rolf",
            "Mohammed",
            "Bryan"
        };
        for (int i = 0; i < names.length; i++) {
            myfriends.add(names[i]);
        }
        myfriends.add("Kalle");
        myfriends.add("Björn");
        myfriends.printFriends();
        System.out.println(myfriends.get(23));
        myfriends.remove("Björn");
        myfriends.printFriends();
        myfriends.addSort("Ahmad");
        myfriends.printFriends();

    }
}