package src;

import java.util.*;

/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
public class CollectionOps {

    public static <T> void print(Collection<T> l) {
        Iterator<T> printer = l.iterator();
        System.out.print("[");
        while (printer.hasNext()) {
            System.out.print(printer.next());
            if (printer.hasNext()) {
                System.out.print(", ");
            } else {
                System.out.print("]");
            }
        }

    }

    public static <T> List<T> reverse(List<T> l) {
        List<T> reverseArray = new ArrayList<T>(l.size());
        for (int i = l.size() - 1; i >= 0; i--) {
            reverseArray.add(l.get(i));
        }
        return reverseArray;
    }

    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<Integer>();
        List<Double> floats = new ArrayList<Double>();
        List<String> names = new ArrayList<String>();

        integers.add(1);
        integers.add(2);
        integers.add(3);
        integers.add(4);
        integers.add(5);

        floats.add(9.7);
        floats.add(3.14);
        floats.add(1.25);

        names.add("Jupiter");
        names.add("Svea");
        names.add("Saga");

        print(integers);
        System.out.println(" reveresed the integers are: " + reverse(integers));
        print(floats);
        System.out.println(" reversed the floats are: " + reverse(floats));
        print(names);
        System.out.println(" reversed the names are: " + reverse(names));
    }
}