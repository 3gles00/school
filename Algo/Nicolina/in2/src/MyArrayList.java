package src;

import java.util.*;

/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
public class MyArrayList<AnyType> implements Iterable<AnyType> {

    private AnyType[] list;
    private int count;

    @SuppressWarnings("unchecked")
    public MyArrayList() {
        list = (AnyType[]) new Object[count];
        count = 0;
    }

    // Correcting array and removing null places in the array
    @SuppressWarnings("unchecked")
    public void remakeArray() {
        int countingValid = 0;
        for (int i = 0; i < list.length; i++) {
            if (list[i] != null) {
                countingValid++;
            }
        }
        AnyType[] temp = (AnyType[]) new Object[countingValid];
        int counting = 0;
        for (int j = 0; j < list.length; j++) {
            if (list[j] != null) {
                temp[counting] = list[j];
                counting++;
            }
        }
        list = temp;
        count = list.length;
    }

    /* Appends the specified element to the end of this list. */
    @SuppressWarnings("unchecked")
    public boolean add(AnyType o) {
        if (o != null) {
            if (count >= list.length) {
                AnyType[] temp = list;
                list = (AnyType[]) new Object[count + 1];
                for (int i = 0; i < count; i++) {
                    list[i] = temp[i];
                }
            }
            list[count] = o;
            count++;
            return true;
        }
        return false;
    }

    /** tests if the specified element is a component of this list */
    public boolean contains(AnyType o) {
        for (int i = 0; i < list.length; i++) {
            if (list[i] == o) {
                return true;
            }
        }
        return false;
    }

    /** returns the component at the specified index */
    public AnyType get(int index) {
        if (index > list.length) {
            return null;
        }
        return list[index];
    }

    /**
     * Search for the first occurrence of the given argument testing for the
     * equality using equals method
     */
    public AnyType indexOf(AnyType o) {
        for (int i = 0; i < count; i++) {
            if (list[i] == o) {
                return o;
            } else {
                System.out.println("Could not find: " + o);
            }
        }
        return null;
    }

    /** tests if this list has no components */
    public boolean isEmpty() {
        if (list.length == 0) {
            return true;
        }
        return false;
    }

    /**
     * removes the first occurrence of the specified element in this list if the
     * list contains the
     */
    public boolean remove(AnyType o) {
        for (int i = 0; i < count; i++) {
            if (list[i] == o) {
                list[i] = null;
                remakeArray();
                return true;
            }
        }
        return false;
    }

    /** returns the number of components in this list */
    public int size() {
        return count;
    }

    /**
     * returns an array containing all elements in this list in the correct order
     */
    public Object[] toArray() {

        @SuppressWarnings("unchecked")
        AnyType[] listCopy = (AnyType[]) new Object[count];
        for (int i = 0; i < count; i++) {
            listCopy[i] = list[i];
        }
        if (list.length == 0) {
            return null;
        }
        return listCopy;
    }

    // return object ArrayListIterator
    public Iterator<AnyType> iterator() {
        Iterator<AnyType> iterating = new ArrayListIterator<AnyType>(list, count);
        return iterating;
    }

}