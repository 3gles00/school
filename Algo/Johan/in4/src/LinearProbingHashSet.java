package src;

import java.util.Iterator;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 4'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class LinearProbingHashSet < Key > {

    public HashElement < Key > [] a;
    private int m = 0;
    private int c = 0;
    private int itr = -1;



    /**
     * Constructor @param m will be the size of the Array
     * @param m
     */
    @SuppressWarnings("unchecked")
    public LinearProbingHashSet(int m) {
        this.m = m;
        this.a = new HashElement[m];
        for (int i = 0; i < m; i++) {
            a[i] = new HashElement < Key > (null);
        }
    }

    /**
     * Constructor will give you a an array with length 2 if no 
     * parameter is given.
     */
    @SuppressWarnings("unchecked")
    public LinearProbingHashSet() {
        this.m = 2;
        this.a = new HashElement[2];
        for (int i = 0; i < m; i++) {
            a[i] = new HashElement < Key > (null);
        }
    }

    /**
     * @return Integer based on the length of the array and @param key
     * @param key
     * @return
     */
    public int hash(Key key) {
        int i = 0;
        String s = key + "";
        for (int j = 0; j < s.length(); j++) {
            i += s.charAt(j);
            i %= a.length;
        }
        return i;
    }

    /**
     * @returns maximum capacity of the array
     * @return
     */
    public int getCapacity() {
        return m;
    }

    public int filled(){
        return c;
    }
    /**
     * Doubles the array and rearranges the hashelements to the new length
     */
    @SuppressWarnings("unchecked")
    private void doubles() {
        this.m = m * 2;
        HashElement < Key > [] temp = a;
        a = new HashElement[m];
        for (int i = 0; i < a.length; i++) {
            a[i] = new HashElement < Key > (null);
        }
        c = 0;
        for (int i = 0; i < temp.length; i++) {
            insert(temp[i].getKey());
            a[findHash(temp[i].getKey())] = new HashElement < Key > (temp[i].getKey(), temp[i].getFrequency());
        }
    }

    /**
     * Inserts the given @param key into the array based on a hash key,
     * Hash key is generated based on the characters of the @param key.
     * @param key
     */
    public void insert(Key key) {
        if (c >= m) doubles();
        if (contains(key)) {
            a[findHash(key)].increment();
        } else {
            int hash = hash(key);
            while (a[hash].getKey() != null) {
                hash++;
                if (hash >= m) hash = 0;
            }
            a[hash] = new HashElement < Key > (key);
            a[hash].increment();
            c++;
        }
    }

    /**
     * Checks if the array contains @param key
     * @reuturn boolean depending on if it exists.
     * @param key
     * @return
     */
    public boolean contains(Key key) {
        for (int i = 0; i < a.length; i++) {
            if (a[i].getKey() == key) return true;
        }
        return false;
    }

    /**
     * Private function uses @param key to return the slot in the array 
     * @param key is in.
     * @param key
     * @return
     */
    private int findHash(Key key) {
        for (int i = 0; i < a.length; i++) {
            if (a[i].getKey() == key) return i;
        }
        return -1;
    }

    /**
     * Decreases the @param key Counter if it's found.
     * @param key
     */
    public void decrease(Key key) {
        if (contains(key)) {
            int hash = findHash(key);
            a[hash].decrement();
            if (a[hash].getFrequency() <= 0) delete(key);
        }
    }

    /**
     * Deletes the key if it exists in the array
     * @param key
     */
    public void delete(Key key) {
        int hash;
        if (contains(key)) {
            hash = findHash(key);
            a[hash] = new HashElement < Key > (null);
        }
    }

    /**
     * Function returns an Iterable that iterates through the list.
     * Function has a boolean returned hasNext() function and a next function.
     * @return
     */
    public Iterable < Key > keys() {
        return new Iterable < Key > () {

            @Override
            public Iterator < Key > iterator() {
                return new Iterator < Key > () {

                    @Override
                    public boolean hasNext() {
                        
                        if (itr + 1 >= m) {
                            itr = -1;
                            return false;
                    
                        }
                        return true;
                    }

                    @Override
                    public Key next() {
                        itr++;
                        if(a[itr].key == null) next();
                        return a[itr].getKey();
                    }
                };
            }
        };
    }

    public static void main(String[] args) {
        LinearProbingHashSet < String > list = new LinearProbingHashSet < String > (2);
        list.insert("KEKW");
        list.insert("OMEGALUL");
        list.insert("LUL");
        while (list.keys().iterator().hasNext()) {
            String temp = list.keys().iterator().next();
            if(temp != null)
            System.out.println(temp);
        }
        System.out.println(list.m);
        System.out.println(list.c);

    }
}   