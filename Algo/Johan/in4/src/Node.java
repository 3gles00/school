package src;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 4'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
class Node{
    
    public int connections, name;
    public double x, y, radius;

    /**
     * Consturctor for a Node
     * @param name
     * @param x
     * @param y
     * @param radius
     */
    public Node(int name, double x, double y, double radius){
        this.name = name;
        this.connections = 0;
        this.x = x;
        this.y = y; 
        this.radius = radius;
    }

    /**
     * Increments the amount of connections current node has. 
     * */ 
    public void increment(){
        connections++;
    }

    /**
     * Checks the connection between current node and @param n.
     * @param n
     * @return
     */
    public boolean connection(Node n){
        if(Math.sqrt(Math.pow(x - n.x, 2) + Math.pow(y - n.y, 2)) <= 
        radius + n.radius) {
            return true;
        }
        return false;
    }

}
