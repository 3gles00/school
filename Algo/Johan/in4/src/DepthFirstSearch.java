package src;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 4'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class DepthFirstSearch {
    private boolean[] marked;
    private int count;

    /**
     * Constructor for the class
     * @param g  
     * @param n 
     * @param m
     */
    public DepthFirstSearch(Graph g, int n, int m){
        marked = new boolean[g.getCapacity()];
        this.count = 0;
        dfs(g, n, m);
    }

    /**
     * Depth First Search searches after @param m 
     * given the Graph @param g and the start point @param n.
     * @param g
     * @param n
     * @param m
     */
    private void dfs(Graph g, int n, int m){
        count++;
        marked[n] = true;
        if(n == m) System.out.println(count());
        int[] adj = g.adj(n);
        for(int w : g.adj(n)){
            if(!marked(w) && adj[w] == 1) {
                dfs(g, w, m);
            }
        }
    }
    /**
     * Checks if the given node has been checked
     * @param w
     * @return
     */
    private boolean marked(int w){
        return marked[w];
    }

    /**
     * @return amount of nodes iterated over. 
     */
    public int count(){
        return count;
    }
}
