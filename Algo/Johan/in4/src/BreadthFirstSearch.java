package src;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 4'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class BreadthFirstSearch{
    private boolean[] marked;
    private int count;
    private Queue<Integer> neighbors;

    /**
     * Constructor goes launches the process.
     * @param g
     * @param n
     */
    public BreadthFirstSearch(Graph g, int n){
        marked = new boolean[g.getCapacity()];
        neighbors = new LinkedList<Integer>();
        this.count = 0;
        bfs(g, n);
    }

    /**
     * Breadth First Search function
     * @param g
     * @param n
     */
    private void bfs(Graph g, int n){
        count++;
        marked[n] = true;
        int[] adj = g.adj(n);
        for(int w : g.adj(n)){
            if(!marked(w) && adj[w] == 1){
                neighbors.add(w);
            }
        }
        for(int w : g.adj(n)){
            if(!marked(w) && adj[w] == 1){
                bfs(g, w);
            }
        }
    }

    /**
     * Checks if element in the array has been gone through
     * @param w
     * @return
     */
    public boolean marked(int w){
        return marked[w];
    }

    /**
     * Returns amount of nodes it has gone through
     * @return
     */
    public int count(){
        return count;
    }

    /**
     * Returns the next node
     * @return
     */
    public int next(){
        return neighbors.poll();
    }
}