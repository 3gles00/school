package src;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 4'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class Graph {
    private Node[] nodes;
    private int[][] connections;
    private  int m, c, totalConnections, networks;

    /**
     * Constructor for class.
     */
    public Graph(){
        this.nodes = new Node[2];
        this.connections = new int[2][2];
        this.m = 2;
        this.c = 0;
        this.totalConnections = 0;
        this. networks = 0;
    }

    /**
     * Private function will resize the arrays if the capacity
     * goes over current @m.
     * @param size
     */
    private void resize(int size){
        Node[] nodes1 = new Node[size];
        int[][] connections1 = new int[size][size];
        for(int i = 0; i < m; i++){
            nodes1[i] = nodes[i];
            for(int j = 0; j < m; j++){
                connections1[i][j] = connections[i][j];
            }
        }
        m = size;
        nodes = nodes1;
        connections = connections1;
    }

    /**
     * Function inserts a node into the array
     * @param n
     */
    public void insert(Node n){
        if(c >= m) resize(m*2);
        nodes[n.name] = n;
        c++;
        drawCircle(nodes[n.name]);
        for(int i = 0; i < c; i++){
            if(n.connection(nodes[i]) && nodes[i].name != n.name){
                totalConnections++;
                nodes[i].increment();
                connections[i][n.name] = 1;
                drawLine(nodes[n.name], nodes[i]); 
            }
            else addNetwork();
        }
    }

    // 
    public void addNetwork(){
        networks++;
    }

    public void checkNetwork(Node n){
        
        
        if(!(networks <= 0)) networks--;
    }

    /**
     * Will override the node placed at @param name in the array.
     * @param name
     */
    public void delete(int name){
        Node temp = nodes[name];
        nodes[name] = null;
        for(int i = 0; i < m; i++){
            connections[i][name] = 0;
            connections[name][i] = 0;
        }
        
        checkNetwork(temp);

    }

    /**
     * @returns the number of nodes in the array.
     */
    public int getCapacity(){
        return c;
    }

    /**
     * @return total amount of connections between nodes.
     */
    public int totalConnections(){
        return totalConnections;
    }

    /**
     * Draws a node using the StdDraw.java class.
     * @param n
     */
    private void drawCircle(Node n){
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(0.001);
        StdDraw.circle(n.x, n.y, n.radius);
    }

    /**
     * Draws a line between two nodes given
     * @param n
     * @param m
     */
    public void drawLine(Node n, Node m){
        StdDraw.setPenColor(StdDraw.BLUE);
        StdDraw.setPenRadius(0.001);
        StdDraw.line(n.x, n.y, m.x, m.y);
    }

    /**
     * Function returns an array of connection to 
     * @param n.
     * @return
     */
    public int[] adj(int n) {
        return connections[n];
    }
    
    /**
     * Functions reades in all of the nodes from a file, path given as @param path.
     * @param path
     * @param width
     * @param height
     */
    public void run(String path, int width, int height){
        int name = -1;
        double x = -1, y = -1, radius = -1;
        StdDraw.setCanvasSize(width, height);
        try{
            // Correct the path
            Scanner sc = new Scanner(new File("school/Algo/Johan/in4/" + path + ".txt"));
            while(sc.hasNext()){
                if(sc.hasNextInt()){
                    if(name != -1){
                        if(x != -1){
                            if(y != -1){
                                radius = (double) sc.nextInt()/1000;
                                // System.out.println(radius);
                                insert(new Node(name, x, y, radius));
                                name = -1;
                                x = -1;
                                y = -1;
                                radius = -1;                                
                            }
                            else y = (double)sc.nextInt()/height;
                        }
                        else x = (double) sc.nextInt()/width;
                    }
                    else name = sc.nextInt();
                }
                else sc.next();
            }
            sc.close();
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
    }
   
    public static void main(String[] args){
        Graph g = new Graph();
        g.run("karta", 1000, 1000);
    }
}
