package src;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 4'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class HashElement<Key> implements Comparable<HashElement<Key>> {

    Key key = null;
    private int counter = 0;

    /**
     * Constructor inserts the @param key
     * @param key
     */
    public HashElement(Key key){
        this.key = key;
    }

    /**
     * Constructor inserts @param key and a @param counter
     * @param key
     * @param counter
     */
    public HashElement(Key key, int counter){
        this.key = key;
        this.counter = counter;
    }

    /**
     * Increments the counter of the HashElement
     */
    public void increment() {
        counter++;
    }

    /**
     * Decreases the counter of the HashElement
     */
    public void decrement() {
        counter--;
    }

    /**
     * @return counter 
     * @return
     */
    public int getFrequency(){
        return counter;
    }

    /**
     * @returns key
     * @return
     */
    public Key getKey(){
        return key;
    }

    /**
     * Swappes key to @param key
     * @param key
     */
    public void setKey(Key key){
        this.key = key;
    }

    /**
     * Compares counters between this counter and given counter.
     */
    @Override
    public int compareTo(HashElement<Key> that) {
        if(counter > that.counter) return 1;
        if(counter < that.counter) return -1;
        return 0;
    }

}
