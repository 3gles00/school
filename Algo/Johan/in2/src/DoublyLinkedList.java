package src;

import java.util.Iterator;
import java.util.Random;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 2'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class DoublyLinkedList<T extends Comparable<T>> implements Iterable<T> {
    
    private Node <T> head;
    private Node <T> tail;
    private Node<T> node;
    private Node<T> currentIt; // Exclusive to Iterator will use
    private int size;

    /**
     * Calls for the LinkedList, setting head and tail to empty for now,
     * also size is set to the -1 to correct the first place in the list later
     */
    public DoublyLinkedList(){
        head = new Node<T>(null);
        tail = head;
        size = 0;
    }

    /**
     * Reorganizes the list starting from the bottom up and reseting the head
     */
    public void reReadList(){
        node = tail;
        while(node.previous != null){
            node = node.previous;
        } 
        head = node;
        currentIt = head;
    }

    /**
     * Adds a node with the information of @param t at the end of the list
     * increments size with 1
     * @param t
     */
    public void add(T t){
        tail.next = new Node<T>(t, null, tail);
        tail = tail.next;
        size++;
        reReadList();   
    }

    /**
     * Adds a node with information of @param t at @param index in the list
     * increments size with 1
     * @param index
     * @param t
     */
    public void add(int index, T t){
        if(index >= size){
            add(t);
        }
        else{
            int count = 0;
            node = head;
            while(node.next != null){
                if(count == index){
                    node.next = new Node<T>(t, node.next, node);
                    node.next.next.previous = node.next;
                }
                count++;
                node = node.next;
            }
            tail = node;
            size++;
            reReadList();
        }
    }

    /**
     * Compares the list to t and adds if the vaulue is higher than t then it would
     * insert it at that place starting from tail and coming to head
     * @param t
     */
    public void addAtFirstSmaller(T t){
        boolean inserted = false;
        if(isEmpty()) add(t);
        else{
            reverse();
            node = head;
            int counter = 0;
            while(node.next != null){
                if(node.next.data.compareTo(t) == -1) {
                    add(counter, t);
                    inserted = true;
                    System.out.println("here");
                    break;
                }
                counter++;
                node = node.next;
            }
            if(!inserted){
                add(t);
            }
            reverse();
        }
    }


    /**
     * Searching the list for @param t and overwrites it once its found, 
     * returns a boolean depending if t was found
     * @param t
     * @return boolean 
     */
    public boolean remove(T t){
        boolean removed = false;
        node = head;
        while(node.next != null){
            if(node.next.data == t){
                if(!removed){
                    node.next = node.next.next;
                    node.next.previous = node;
                    removed = true;
                }
            }
            node = node.next;
        }
        tail = node;
        size--;
        reReadList();
        return removed;
    }

    /**
     * Looks at the @param index seat in the list and pops it out for return
     * @param index
     * @return T
     */
    public T remove(int index){
        node = head;
        if(index > size){
            System.out.println("Index Out of Bonds");
            return null;
        }
        else{
            int count = 0;
            while(count < size){
                node = node.next;
                count++;
            }
            remove(node.data);
            return node.data;
        }
    }

    /**
     * Reverses the list using MyStack.java
     */
    public void reverse(){
        MyStack<T> stack = new MyStack<T>();
        currentIt = head;
        while(iterator().hasNext()){
            stack.push(iterator().next());
        }
        clear();
        while(!stack.isEmpty()){
            add(stack.pop());
        }
    }

    
    /**
     * Iterator Iterating the list
     */
    @Override
    public Iterator<T> iterator()
    {
        return new Iterator<T>()
        {
            public boolean hasNext()
            {
                return currentIt.next != null;
            }
            
            public T next()
            {
                currentIt = currentIt.next;
                return currentIt.data;
            }
        };
    }

    /**
     * remakes the list into array String format
     */
    public String toString(){
        String s = "[ ";
        currentIt = head;
        while(iterator().hasNext()) {
            s += iterator().next() + " ";
        }
        s += "]";
        return s;
    }

    /**
     * Pops the first element if the list and returns its information
     * @return
     */
    public T removeFirst(){
        return remove(0);
    }

    /**
     * Pops out the last element of the list and returns the information in it
     * @return
     */
    public T removeLast(){
        return remove(size);
    }

    /**
     * Returns the first element inforamtion in the list 
     * @return
     */
    public T getFirst(){
        return head.next.data;
    }

    /**
     * Returns the last element in the list
     * @return
     */
    public T getLast(){
        return tail.data;
    }

    /**
     * Returns a boolean depending 
     * @return
     */
    public boolean isEmpty(){
        return size == 0;
    }

    /**
     * Returns size
     * @return
     */
    public int size(){
        return size + 1;
    }

    /**
     * Overrites the list
     */
    public void clear(){
        head = new Node<T>(null);
        tail = head;
        size = 0;
    }

    public static void main(String[] args){
        DoublyLinkedList<Integer> list = new DoublyLinkedList<Integer>();
        Random rand = new Random();
        for(int i = 0; i < 5; i++)
        list.addAtFirstSmaller(rand.nextInt(25) + 1);
        System.out.println(list.toString());
        System.out.println(list.size());
        list.remove(0);
        System.out.println(list.toString());
        System.out.println(list.size());
    }
}
