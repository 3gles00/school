package src;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 2'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class MyStack<T extends Comparable<T>>{

    private Node<T> topOfStack;

    public MyStack(){
        topOfStack = null;
    }

    public boolean isEmpty(){
        return topOfStack == null;
    }

    public T peek(){
        return topOfStack.data;
    }

    public T pop(){
        Node<T> node = topOfStack;
        topOfStack = topOfStack.next;
        return node.data;
    }

    public void push(T t){
        Node<T> node = topOfStack;
        topOfStack = new Node<T>(t, node);
    }
}
