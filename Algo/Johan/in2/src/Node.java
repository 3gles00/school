package src;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 2'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class Node<T> {
    
    public T data;
    public Node<T> next = null;
    public Node<T> previous = null;

    public Node(T newData){
        this.data = newData;
    }

    public Node(T newData, Node<T> next){
        this.data = newData;
        this.next = next;
        
    }

    public Node(T newData, Node<T> next, Node<T> previous){
        this.data = newData;
        this.next = next;
        this.previous = previous;
    }
}
