package src;

import java.util.Comparator;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 1'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
class PopulationCompare implements Comparator<Country>{

    @Override
    public int compare(Country o1, Country o2) {
        if(o1.population < o2.population) return -1;
        if(o1.population > o2.population) return 1;
        return 0;
    }

    
}