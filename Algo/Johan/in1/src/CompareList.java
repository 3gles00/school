package src;

import java.util.Comparator;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 1'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class CompareList {
    
    public static MyArrayList<Country> findMinMax(MyArrayList<Country> list){
        MyArrayList<Country> answer = new MyArrayList<Country>();
        Country min = list.get(0);
        Country max = list.get(0);
        for(int i = 0; i < list.size(); i++){
            Country temp = list.get(i);
            for(int j = 0; j < temp.name.length(); i++){
                if(temp.name.charAt(i) < min.name.charAt(i)){
                    min = temp;
                }
                if(temp.name.charAt(i) > min.name.charAt(i)){
                    max = temp;
                }
            }
        }
        answer.add(min);
        answer.add(max);
        return answer;
    }

    public static MyArrayList<Country> findMinMax(MyArrayList<Country> list, Comparator<Country> c){
        MyArrayList<Country> temp = new MyArrayList<Country>();
        Country min = list.get(0);
        Country max = list.get(0);
        for(int i = 0; i < list.size(); i++){
            if (c.compare(list.get(i), min) < 0){
                min = list.get(i);
            }
            if(c.compare(list.get(i), max) > 0){
                max = list.get(i);
            }
        }
        temp.add(min);
        temp.add(max);
        return temp;
    }

    public static void main(String[] args){
        MyArrayList<Country> list = new MyArrayList<Country>();
        list.add(new Country("Sweden", "Stockholm", 10230000));
        list.add(new Country("Norway", "Oslo", 5300000));
        list.add(new Country("Denmark", "Copenhamn", 5800000));
        list = findMinMax(list, new PopulationCompare());
        System.out.println(list.toString());        
        
    }
}
