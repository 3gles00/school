package src;

import java.util.*;

/**
 * Class contians part of assignments from the course
 * 'Algorithms and Computer Science' which is taught at
 * Halmstad University
 * @Author: Haron Obaid
 */
public class MyArrayList<T> {

    private T[] list;
    private int count;
    private int itr = -1;

    @SuppressWarnings("unchecked")
    public MyArrayList() {
        list = (T[]) new Object[10];
        count = 0;
    }

    // Correcting array and removing null places in the array
    @SuppressWarnings("unchecked")
    public void remakeArray() {
        int countingValid = 0;
        for (int i = 0; i < list.length; i++) {
            if (list[i] != null) {
                countingValid++;
            }
        }
        T[] temp = (T[]) new Object[countingValid];
        int counting = 0;
        for (int j = 0; j < list.length; j++) {
            if (list[j] != null) {
                temp[counting] = list[j];
                counting++;
            }
        }
        list = temp;
        count = list.length;
    }

    /* Appends the specified element to the end of this list. */
    @SuppressWarnings("unchecked")
    public boolean add(T o) {
        if (o != null) {
            if (count >= list.length) {
                T[] temp = list;
                list = (T[]) new Object[count + 1];
                for (int i = 0; i < count; i++) {
                    list[i] = temp[i];
                }
            }
            list[count] = o;
            count++;
            return true;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    public void add(int index, T o){
        count++;
        if(list[index] != null){
            if(count >= list.length){
                T[] temp = list;
                list = (T[]) new Object[count];
                int j = 0;
                for(int i = 0; i < count; i++){
                    if(index == i){
                        list[i] = o;
                        i++;
                    }
                    list[i] = temp[j];
                    j++;
                }
            }
        }
        else{
            list[index] = o;
        }
    }

    /** tests if the specified element is a component of this list */
    public boolean contains(T o) {
        for (int i = 0; i < list.length; i++) {
            if (list[i] == o) {
                return true;
            }
        }
        return false;
    }

    /** returns the component at the specified index */
    public T get(int index) {
        if (index > list.length) {
            return null;
        }
        return list[index];
    }

    public T set(int index, T o){
        T temp = list[index];
        list[index] = o;
        return temp;
    }

    /**
     * Search for the first occurrence of the given argument testing for the
     * equality using equals method
     */
    public T indexOf(T o) {
        for (int i = 0; i < count; i++) {
            if (list[i] == o) {
                return o;
            } else {
                System.out.println("Could not find: " + o);
            }
        }
        return null;
    }

    /** tests if this list has no components */
    public boolean isEmpty() {
        if (list.length == 0) {
            return true;
        }
        return false;
    }

    /**
     * removes the first occurrence of the specified element in this list if the
     * list contains the
     */
    public boolean remove(T o) {
        for (int i = 0; i < count; i++) {
            if (list[i] == o) {
                list[i] = null;
                remakeArray();
                return true;
            }
        }
        return false;
    }

    /** returns the number of components in this list */
    public int size() {
        return count;
    }

    public void clear(){
        for(int i = 0; i < count; i++){
            list [i] = null;
        }
    }

    /**
     * returns an array containing all elements in this list in the correct order
     */
    public Object[] toArray() {
        return list;
    }

    public String toString(){
        String s = "[";
        while(iterator().hasNext()){
            s += iterator().next() + "";
            if(iterator().hasNext()){
                s += ", ";
            }
        }
        s += "]";
        itr = -1;
        return s;
    }

    // return object ArrayListIterator
    public Iterator<T> iterator() {
        return new Iterator<T>(){
            @Override
            public boolean hasNext() {
                return list[itr+1] != null;
            }


            @Override
            public T next() {
                itr++;
                return list[itr];
            }
        };
    }

    public static void main(String[] s){
        MyArrayList<String> strings = new MyArrayList<String>();
        strings.add("help");
        strings.add("me");
        strings.add("please");
        System.out.println(strings.toString());
    }
}
