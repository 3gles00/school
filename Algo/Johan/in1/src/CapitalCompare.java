package src;

import java.util.Comparator;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 1'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class CapitalCompare implements Comparator<Country>{

    @Override
    public int compare(Country o1, Country o2) {
        int length = o1.capital.length();
        if(o1.capital.length() > o2.capital.length()) length = o2.capital.length();
        
        for(int i = 0; i < length; i++){
            if(o1.capital.charAt(i) < o2.capital.charAt(i)) return 1;
            if(o1.capital.charAt(i) > o2.capital.charAt(i)) return -1;
        }
        return 0;
    }

}
