package src;

import java.util.Comparator;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 1'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class NameCompare implements Comparator<Country>{

    @Override
    public int compare(Country o1, Country o2) {
        int length = o1.name.length();
        if(o1.name.length() > o2.name.length()) length = o2.name.length();
        
        for(int i = 0; i < length; i++){
            if(o1.name.charAt(i) < o2.name.charAt(i)) return 1;
            if(o1.name.charAt(i) > o2.name.charAt(i)) return -1;
        }
        return 0;
    }
    
}
