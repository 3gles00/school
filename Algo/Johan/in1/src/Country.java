package src;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 1'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class Country implements Comparable<Country>{
    
    String name;
    String capital;
    int population;

    public Country(String name, String capital, int population){
        this.name = name;
        this.capital = capital;
        this.population = population;
    }

    @Override
    public int compareTo(Country o) {
        if(o.population < population) return -1;
        if(o.population > population) return 1;
        return 0;
    }


}
