package src;

import java.io.*;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 3'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class Trader extends Thread {
    DataBuffer < StockPick > stockPicks;

    // Fler intstansvariabler?

    int nrPicks; // nr of stock picks for printing to log-file each second
    int endTime; // time to run in seconds


    // Constructor
    public Trader(DataBuffer < StockPick > stockPicks,
        int bufferSize, int nrPicks, int endTime) {
        this.stockPicks = stockPicks;
        this.nrPicks = nrPicks;
        this.endTime = endTime;
    }

    private synchronized StockPick dequeue(){
        return stockPicks.dequeue();
    }

    private synchronized void enqueue(StockPick stock){
        if(stock != null){    
            int count = -1;
            while(stockPicks.hasNext()){
                StockPick temp = stockPicks.next();
                count++;
                if(temp.compareTo(stock) == -1){
                    stockPicks.insert(stock, count);
                    break;
                }
            }
        } 
    }

    private void pq(){
        StockPick[] a = new StockPick[stockPicks.bufferSize()];
        int count = -1;
        while(!stockPicks.isEmpty()){
            a[count++] = dequeue();
        }
        for(StockPick i : a){
            enqueue(i);
        }
    }

    /**
     * Function takes the Values of DataBuffer and prints them in the file
     * log.txt
     */
    public void run() {
        try {
            OutputStreamWriter writer =
                new OutputStreamWriter(new FileOutputStream("school/Algo/Johan/in3/log.txt", true));
            writer.write("Start\n");
            pq();
            int time = 0;
            while (time < endTime) {
                
                
                try {
                    sleep(1000);
                } catch (InterruptedException e) {}
                
                /*
                * LÃ¤gg till kod hÃ¤r.
                * 
                * 1: TÃ¶m stockPicks och lÃ¤gg elementen i en tom priortietskÃ¶. 
                * Denna prioritetskÃ¶ kan t.ex. vara en instansvariabel och initieras i 
                * konstruktorn.
                * 
                * 2: Ta de nrPicks stÃ¶rsta elementen frÃ¥n prioritetskÃ¶n och skriv 
                * ut dessa i prioritetsordning sist i log.txt. Ni ska inte skriva 
                * Ã¶ver det som finns i filen utan lÃ¤gga till pÃ¥ slutet.
                */
                
                writer.append(dequeue() + "\n");
                
                time++;
                
                System.out.println("Time elapsed: " + time +
                " seconds.");
            }
            writer.close();
        } catch (IOException e) {}
    }


    public static void main(String[] cmdLn) {
        int bufferSize = 50;
        DataBuffer < StockPick > stockPicks =
            new DataBuffer < StockPick > (bufferSize);

        // StockPicker 1
        String[] stocks1 = new String[] {
            "TSLA",
            "CCJ",
            "GME",
            "UUUU",
            "MFST",
            "GOOGL",
            "AAPL",
            "AMZN"
        };

        StockPicker Stockpicker1 =
            new StockPicker("North America analyzer",
                stockPicks, stocks1, 10);


        // StockPicker 2
        String[] stocks2 = new String[] {
            "ETH",
            "BTC"
        };

        StockPicker Stockpicker2 =
            new StockPicker("Cryptocurrencices analyzer",
                stockPicks, stocks2, 10);

        // trader
        Trader trader = new Trader(stockPicks, bufferSize, 3, 10);

        // run simulation
        Stockpicker1.start();
        Stockpicker2.start();
        trader.start();
    }
}