package src;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 3'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class Node<T> {
    
    public T data;
    public Node<T> next = null;
    public Node<T> previous = null;
    public int priority; 

    public Node(T newData){
        this.data = newData;
    }

    public Node(T newData, Node<T> next){
        this.data = newData;
        this.next = next;
    }

    public Node(T newData, Node<T> next, Node<T> previous){
        this.data = newData;
        this.next = next;
        this.previous = previous;
    }

    public Node(T newData, int priority){
        this.data = newData;
        this.priority = priority;
    }

    public Node(T newData, Node<T> next, int priority){
        this.data = newData;
        this.next = next;
        this.priority = priority;
    }

    public Node(T newData, Node<T> next, Node<T> previous, int priority){
        this.data = newData;
        this.next = next;
        this.previous = previous;
        this.priority = priority;
    }
}
