package src;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 3'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class TreeNode<T> {
    
    private T data;
    public TreeNode<T> parent;
    public TreeNode<T> left;
    public TreeNode<T> right;
    private int counter; 

    public TreeNode(T newData) {
        this.data = newData;
        this.counter = 1;
    }

    public TreeNode(T newData, int counter){
        this.data = newData;
        this.counter = counter;
    }


    public TreeNode(T newData, TreeNode<T> right, TreeNode<T> left, TreeNode<T> parent){
        this.data = newData;
        this.left = left;
        this.left = right;
        this.parent = parent;
        this.counter = 1;
    }

    public T getData(){
        return data;
    }

    public int getCounter(){
        return counter;
    }

    public void increment(){
        counter++;
    }
}
