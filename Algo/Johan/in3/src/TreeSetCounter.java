package src;

import java.util.Iterator;
// import java.util.NoSuchElementException;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 3'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class TreeSetCounter<T extends Comparable<T>> implements Iterable<T> {
    
    TreeNode<T> root;
    TreeNode<T> next;    
    int size;

    /**
     * Contructor creates TreeSetCounter as a Binary Tree
     */
    public TreeSetCounter(){
        root = new TreeNode<T>(null);
        size = 0;
    }

    /**
     * Inserts @param t into the TreeSetCounter using a recursive method
     * @param t
     */
    public void add(T t){
        next = root = addRecursive(root, t);
        
    }

    /**
     * A recursive method that inserts @param t into the Tree.
     * @param current
     * @param t
     * @return A TreeNode 
     */
    private TreeNode<T> addRecursive(TreeNode<T> current, T t){
        if(current.getData() == null) return new TreeNode<T>(t);
        if(t.compareTo(current.getData()) == -1) {
            current.left.parent = current;
            current.left = addRecursive(current.left, t);
        }
        else if(t.compareTo(current.getData()) == 1){
            current.left = new TreeNode<T>(null);
            current.parent = current;
            current.left = addRecursive(current.left, t);
        } 
        else current.increment();
        size++;
        return current;
    }

    /**
     * Clears the tree by overwriting root TreeNode.
     */
    public void clear(){
        root = new TreeNode<T>(null);
        size = 0;
    }

    /**
     * Method that takes and @return the most frequent items count.
     * @return
     */
    public int getMaxFrequency(){
        int maxCount = 0;
        int check = 0;
        TreeNode<T> node = mostLeft();
        if(node.getCounter() > maxCount) maxCount = node.getCounter();
        while(node.parent != null || node.right != null){
            if(node.right != null){
                node = node.right;
                while(node.left != null) node = node.left;
            }
            else{
                node = node.parent;    
            }
            if(maxCount < node.getCounter()) maxCount = node.getCounter();
            check++;
            if(check == size) break;
        }
        return maxCount;
    }

    /**
     * Checks if @param t is inserted into the Tree.
     * Function can be used outside of TreeSetCounter.java
     * @param t
     * @return
     */
    public boolean contains(T t){
        if(contains(root, t).getData() == t) return true;
        return false;
    }

    /**
     * Function that checks if the @param t is inserted within the Tree
     * Function can only be used within TreeSetCounter.java
     * @param current
     * @param t
     * @return
     */
    private TreeNode<T> contains(TreeNode<T> current, T t){
        if(t.equals(null)) return null;
        if(t.equals(current.getData())) return current;
        return t.compareTo(current.getData()) == -1
        ? contains(current.left, t)
        : contains(current.right, t);
    }

    /**
     * Checks if the Tree is empty.
     * @return
     */
    public boolean isEmpty(){
        return root.getData() == null;
    }

    /**
     * Returns the size of the Tree.
     * @return
     */
    public int size(){
        return size;
    }

    /**
     * Returns the amount of times @param t is used
     * @param t
     * @return
     */
    public int counter(T t){
        return contains(root, t).getCounter(); 
    }

    /**
     * Returns the Lowest element in the Tree based on Comparable.
     * @return
     */
    public TreeNode<T> mostLeft(){
        TreeNode<T> temp = root;
        while(temp.left != null) temp = temp.left;
        return temp;
    }

    /**
     * Returns an Iterator iterating over the elements.
     */
    @Override
    public Iterator<T> iterator() {
        if(next == null){
            next = mostLeft();
        }
        return new Iterator<T>(){

            /**
             * Checks if there is a node past the current one
             */
            @Override
            public boolean hasNext() {
                return next() != null;
            }

            /**
             * Returns the next element in the Tree
             */
            @Override
            public T next() {
                if(next.right != null){
                    next = next.right;
                    while(next.left != null) next = next.left;
                    return next.getData();
                }
                next = next.parent;
                return next.getData();
            }

        };
    }    

    public static void main(String[] args){
        TreeSetCounter<Integer> counter = new TreeSetCounter<Integer>();
        counter.add(1);
        counter.add(2);
        counter.add(3);

        while(counter.iterator().hasNext()){
            System.out.println(counter.iterator().next());
        }
    }
}
