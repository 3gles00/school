package src;

import java.util.Iterator;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 3'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class DataBuffer<T> implements Iterator<T>{

    private Node<T> head, tail, itr;
    private int maxCapacity, size;

    // Constructor
    public DataBuffer(int maxCapacity){
        this.maxCapacity = maxCapacity;
        this.size = 0; 
        head = new Node<T>(null);
        tail = head;
    }

    /**
     * Function will read the list from tail to head and correct it.
     */
    private void reReadList(){
        Node<T> node = tail;
        while(node.data != head.data) node = node.previous;
        head = node;
        itr = head;
    }

    /**
     * Function enqueues the element in the Databuffer at the end
     * Will print the Limited size if you try to enqueue more elements
     * than max size
     * @param t
     */
    public void enqueue(T t){
        if(isFull()){
            System.out.println("Limited Size " + size);
        }
        else{
            if(t != null){
                tail.next = new Node<T>(t, head, tail);
                tail = tail.next;
                head.previous = tail;
                size++;
                reReadList();
            }
        }
    }

    public void insert(T t, int index){
        if(isFull()){
            System.out.println("Limited Size " + size);
        }
        else{
            if(t != null){
               if(index < size){
                    int count = 0;
                    Node<T> node = head;
                    Node<T> temp;
                    while(count < index){
                        node = node.next;
                        count++;
                    }
                    temp = node.next;
                    node.next = new Node<T>(t, temp, node);
                    node.next.next.previous = node.next;
                    size++;
                    count++;
                    while(node != tail){
                        node = node.next;
                    }
                    tail = node;
                    reReadList();
               }
               else{
                   enqueue(t);
               } 
           }
        }
    }

    /**
     * Function dequeues the first element in the buffer
     * @return
     */
    public T dequeue(){
        Node<T> temp = head.next;
        head.next = head.next.next;
        tail.next = head;
        size--;
        reReadList();
        return temp.data;
    }

    /**
     * Changes the maximum size of the buffer will delete the latest added
     * elements in the buffer if new max size is lower than old size
     * @param maxSize
     */
    public void changeSize(int maxSize){
        while(isFull()){
            tail = tail.previous;
            tail.next = head;
            head.previous = tail;
            reReadList();
            size--;

        }
        this.maxCapacity = maxSize;
        
    }

    /**
     * Checks if the buffer is full by seeing if the
     * current capacity is equal or larger than max 
     * capacity
     * @return
     */
    public boolean isFull() {
        return size >= maxCapacity;
    }

    /**
     * Checks if buffer is empty
     * @return
     */
    public boolean isEmpty(){
        return size == 0;
    }

    /**
     * Returns current capacity of the buffer
     * @return
     */
    public int size(){
        return size;
    }

    /**
     * Returns the maximum capacity of the 
     * buffer
     * @return
     */
    public int bufferSize(){
        return maxCapacity;
    }

    /**
     * Returns the state of the iterator and if it has a next element to return.
     * @return
     */
    @Override
    public boolean hasNext() {
        if(itr.next.data == null){
            itr = head;
            return false;
        }
        return true;
    }

    /**
     * Returns the next iterating item in the list.
     * @return
     */
    @Override
    public T next() {
        itr = itr.next;
        return itr.data;
    }

    public static void main(String[] args) {
        DataBuffer < String > buff = new DataBuffer<String>(2);
        buff.insert("dude", 1);
        buff.insert("buff", 1);
        buff.insert("buff", 1);
        while(buff.hasNext()){
            System.out.println(buff.next());
        }

    }
}
