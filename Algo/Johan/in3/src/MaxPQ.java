package src;

import java.util.*;

/**
 * Class contians a part of the assignments 'Inlamningsuppgifter 3'
 * from the course 'Algorithmes and Computer Structure' which is taught at
 * 'Hogskolan i Halmstad'
 * @Author: Haron Obaid 
 */
public class MaxPQ < T extends Comparable < T >> {
    T[] a;
    int size = 0;
    int dequeueCounter = 0;
    public Object dataOut;

    /**
     * Constructor for the MaxPQ
     * default size is 10
     */
    @SuppressWarnings("unchecked")
    public MaxPQ() {
        a = (T[]) new Comparable[10];
        size = 0;
    }

    /**
     * Constructor for the MaxPQ 
     * @param max sets the maximum size
     */
    @SuppressWarnings("unchecked")
    public MaxPQ(int max) {
        a = (T[]) new Comparable[max];
        size = 0;
    }

    /**
     * Constructor for PQ inserts given 
     * @param b as PQ.
     */
    @SuppressWarnings("unchecked")
    public MaxPQ(T[] b) {
        if(isEmpty()){
            a = (T[]) new Comparable[10];
            size = 0;
        }
        for (int i = 0; i < b.length; i++) {
            insert(b[i]);
        }
    }

    /**
     * Return size of the MaxPQ 
     * @return
     */
    public int size() {
        return size;
    }

    /**
     * Checks if the MaxPQ is empty
     * @return
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Inserts the
     * @param t into the MaxPQ
     */
    public void insert(T t) {
        // array doubling 
        if (size == a.length - 2)
            a = Arrays.copyOf(a, 2 * a.length);

        a[++size] = t;

        swim(size);
    }

    /**
     * Function lets element swim up
     * @param k
     */
    private void swim(int k) {
        while (k / 2 > 0 && less(k / 2, k)) {
            exchange(k / 2, k);
            k /= 2;
        }
    }

    /**
     * Compares two 
     * @param i
     * @param j
     * @return boolean
     */
    private boolean less(int i, int j) {
        return a[i].compareTo(a[j]) < 0;
    }

    /**
     * Changes the two values in the MaxPQ
     * @param i
     * @param j
     */
    private void exchange(int i, int j) {
        T temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    /**
     * Removes and
     * @return returns biggest value
     */
    public T delMax() {
        if (size == 0) return null;
        T maxElement = a[0];
        exchange(0, size);
        a[size--] = null;
        if (size > 1) sink(1);
        return maxElement;
    }

    /**
     * Function makes given number in the array
     * to sink down into the array.
     * @param k
     */
    public void sink(int k) {
        while (2 * k <= size) {
            int j = 2 * k;
            if (j < size && less(j, j + 1)) j++;
            if (less(j, k)) break;
            exchange(j, k);
            k = j;
        }
    }

    /**
     * Returnes first value(highest value in the array)
     * @return
     */
    public T max() {
        if (size == 0) return null;
        return a[0];
    }

    /**
     * Returnes first value in the Array
     * @return
     */
    public T dequeue(){
        T temp = a[dequeueCounter];
        a[dequeueCounter] = null;
        dequeueCounter++;
        return temp;
    }


    public static void main(String[] args){
        Integer[] a = {1,2,3,4,5,6,7,8,};

    }
}